#ifndef _DEFERRED_RENDERER_H_
#define _DEFERRED_RENDERER_H_

#include "GLDefines.h"

#include "GLRenderer.h"
#include "VolumeRenderer.h"
#include "ResourceLoader.h"
#include "Light.h"
#include "PointLIght.h"
#include "SpotLight.h"

class DeferredRenderer
{
public:
	DeferredRenderer(GLRenderer* renderer, ResourceLoader* loader);
	~DeferredRenderer();

	void AddLight(PointLight* light);
	void AddLight(SpotLight* light);
	void DrawMesh(MeshPtr mesh, Camera* cam);

	void BeginFrame();
	void EndFrame();

	void BeginRenderGBuffer();
	void EndRenderGBuffer();

	void BeginRenderPointLights();
	void BeginRenderSpotLights();
	void EndRenderLights();

	void BeginRenderSpotShadows(SpotLight* light, Camera* cam);
	void RenderShadowMesh(MeshPtr mesh);
	void EndRenderSpotShadows();

	void RenderDebugView();

	void RenderPost();
	void EndRenderPost();

	void BeginApplyLighting();
	void EndApplyLighting();

	void ApplyIndirect();

	void BeginLumaConvert();
	void BeginTonemap();
	void RenderFinalScene();

	ShaderPtr GetGBufferVS();
	ShaderPtr GetGBufferPS();
	ShaderPtr GetGBufferDebugPS();

	ShaderPtr GetPointLightingPS();
	ShaderPtr GetSpotLightingPS();
	ShaderPtr GetDepthPassVS();
	ShaderPtr GetDepthPassPS();

	std::vector<TexturePtr> GetGBuffer();
	std::vector<PointLight*> GetPointLights();
	std::vector<SpotLight*> GetSpotLights();
	
	TexturePtr GetHDRLItScene();
	TexturePtr GetFinalScene();
	TexturePtr GetShadowmap();


private:
	GLRenderer* m_Renderer;
	ivec2 m_Size;
	ResourceLoader* m_Loader;
	VolumeRenderer* m_VolumeRenderer;

	std::vector<TexturePtr> m_GBuffer;
	std::vector<PointLight*> m_PointLights;
	std::vector<SpotLight*> m_SpotLights;

	FramebufferPtr m_GBufferFBO;

	RenderTarget2D* m_LightBuffer;
	TexturePtr m_LumaBuffer;
	TexturePtr m_HDRLitScene;
	TexturePtr m_FinalScene;

	RenderTarget2D* m_SpotShadowMap;

	ShaderPtr m_GBufferVS;
	ShaderPtr m_GBufferPS;
	ShaderPtr m_GBufferDebug;
	ShaderPtr m_ShadowDebug;

	ShaderPtr m_PointLighting;
	ShaderPtr m_SpotLighting;

	ShaderPtr m_ApplyLighting;
	ShaderPtr m_LumaConvert;
	ShaderPtr m_Tonemap;

	ShaderPtr m_SpotShadowVS;
	ShaderPtr m_SpotShadowPS;

	TexturePtr m_CheckerTexture;

};
#endif