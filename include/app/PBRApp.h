#pragma once
#ifndef _PBR_APP_H_
#define _PBR_APP_H_
#include "GLApp.h"
#include "DeferredRenderer.h"
#include "OITRenderer.h"

class PBRApp : public GLApp
{
public:
	PBRApp();
	void Init(string root_dir) override;
	void Destroy() override;

	void Run() override;

	void RenderFrame() override;

protected:

	bool lock_cam_light = false;
	bool conservative_raster = true;

	float cam_light_intensity = 500000.0f;
	float cam_light_radius = 2500.0f;
	vec3 cam_light_color = vec3(1.0f);

	enum VolumeDir
	{
		NEG_X,
		NEG_Y,
		NEG_Z,
		POS_X,
		POS_Y,
		POS_Z,
	};

	VolumeDir vis_dir = POS_X;


	enum Visualization
	{
		SCENE,
		G_BUFFER,
		OIT,
		AO,
		DIFFUSE,
		SPECULAR,
		EMITTANCE,
		OPACITY
	};
	
	Visualization debug_view = SCENE;

	s32 voxel_mip = 0;

	PolygonMode poly_mode = PM_FILL;

#if defined(CAM_LIGHT_POINT)
	PointLight* m_CamLight;
#elif defined(CAM_LIGHT_SPOT)
	SpotLight* m_CamLight;
#endif

	DeferredRenderer* m_DeferredRenderer;
	VolumeRenderer* m_VolumeRenderer;
	OITRenderer* m_OIT;

	Entity* m_Scene;
	Entity* m_TeapotEntity;
	MeshPtr m_Sponza;
	MeshPtr m_Cube;
	MeshPtr m_Teapot;
	MeshPtr m_CornellBox;

	TexturePtr m_Volume;
};
#endif