#pragma once
#ifndef _VOLUME_RENDERER_H_
#define _VOLUME_RENDERER_H_

#include "GLRenderer.h"
#include "ResourceLoader.h"
#include "SpotLight.h"

class VolumeRenderer
{
public:
	VolumeRenderer(GLRenderer* renderer, ResourceLoader* loader, ivec3 size);
	~VolumeRenderer();

	void BeginVoxelize();
	void EndVoxelize();

	void VoxelizeMeshOpacity(MeshPtr mesh, f32 alpha = 1.0f);
	void VoxelizeMeshEmittance(MeshPtr mesh);
	void VoxelizeMeshEmittance(MeshPtr mesh, SpotLight* light);

	void VoxelizeMeshEmissive(MeshPtr mesh);

	void FilterEmittance();
	void DownsampleOpacity();
	void DownsampleEmittance();

	void DrawTraceBounds(Camera* cam);

	void RaytraceEmittance(Camera* cam, s32 index, s32 level = 0);
	void RayTraceAnisotropicEmittance(Camera* cam);

	void RaytraceOpacity(Camera* cam, s32 index, s32 level = 0);

	void TraceAO(std::vector<TexturePtr> g_buffer, s32 nth_pixel, Camera* cam);
	void TraceDiffuseAO(std::vector<TexturePtr> g_buffer, s32 nth_pixel, Camera* cam);
	void TraceSpecular(std::vector<TexturePtr> g_buffer, s32 nth_pixel, Camera* cam);
	
	void GatherIndirect(std::vector<TexturePtr> g_buffer, s32 nth_pixel, Camera* cam);

	void DepthAwareBlur();

	TexturePtr GetResolvedTrace();
	TexturePtr GetResolvedAO();
	TexturePtr GetResolvedDiffuse();
	TexturePtr GetResolvedSpecular();

	std::vector<TexturePtr> GetOpacityVolumes();
	std::vector<TexturePtr> GetEmittanceVolumes();

	void ClearVolume();
	void SetVolumeExtents(vec4 ext);
	void SetNearPlane(f32 near);
	void SetFarPlane(f32 far);
	void SetVolumePosition(vec3 pos);

	void SetConservativeRaster(bool state);
	void SetOpacityDirty(bool dirty);
	void SetEmittanceDirty(bool dirty);

	void Blur(TexturePtr input, TexturePtr output, vec2 direction);

	float GetVoxelExtents();
	s32 GetVoxelResolution();
	s32 GetMipCount();
	void BindVolumes();

private:

	GLRenderer* m_Renderer;

	bool m_ConservativeRaster;
	bool m_OpacityDirty;
	bool m_EmittanceDirty;

	RenderTarget2D* m_Front;
	RenderTarget2D* m_Back;
	RenderTarget2D* m_TraceRT;

	TexturePtr m_Trace;
	TexturePtr m_BlurIntermediate;

	MeshPtr m_Cube;

	ivec3 m_GridSize;
	ivec2 m_Size;

	vec3 m_GridScale;
	vec4 m_WorldExtents;

	OrthoCamera* m_NegXCam;
	OrthoCamera* m_PosXCam;

	OrthoCamera* m_NegYCam;
	OrthoCamera* m_PosYCam;

	OrthoCamera* m_NegZCam;
	OrthoCamera* m_PosZCam;

	TexturePtr m_NegXEmittance;
	TexturePtr m_PosXEmittance;

	TexturePtr m_NegYEmittance;
	TexturePtr m_PosYEmittance;

	TexturePtr m_NegZEmittance;
	TexturePtr m_PosZEmittance;

	TexturePtr m_NegXOpacity;
	TexturePtr m_PosXOpacity;

	TexturePtr m_NegYOpacity;
	TexturePtr m_PosYOpacity;

	TexturePtr m_NegZOpacity;
	TexturePtr m_PosZOpacity;

	TexturePtr m_CheckerTexture;

	std::vector<TexturePtr> m_EmittanceVolumes;
	std::vector<TexturePtr> m_OpacityVolumes;
	std::vector<OrthoCamera*> m_VolumeCameras;

	ShaderPtr m_ShellVS;
	ShaderPtr m_ShellPS;


	ShaderPtr m_VoxelizeVS;
	ShaderPtr m_VoxelizeGS;
	ShaderPtr m_VoxelizeOpacityPS;
	ShaderPtr m_VoxelizeEmittancePS;

	ShaderPtr m_FilterEmittanceCS;

	ShaderPtr m_DownsampleOpacityCS;
	ShaderPtr m_DownsampleEmittanceCS;

	ShaderPtr m_TraceAO;
	ShaderPtr m_TraceDiffuse;
	ShaderPtr m_TraceShadow;
	ShaderPtr m_TraceSpecular;
	ShaderPtr m_GatherIndirect;

	ShaderPtr m_BlurCS;

	TexturePtr m_AOResolve;
	TexturePtr m_DiffuseResolve;
	TexturePtr m_SpecularResolve;


	ShaderPtr m_BoundsView;
	ShaderPtr m_VolumeClear;

	ShaderPtr m_VolumeTraceCS;
	ShaderPtr m_EmittanceTraceCS;

	Entity* m_DebugCube;

	const s32 MIPS = 7;
};
#endif