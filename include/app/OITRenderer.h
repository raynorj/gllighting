#ifndef _OIT_RENDERER_H_
#define _OIT_RENDERER_H_

#include "GLRenderer.h"
#include "ResourceLoader.h"

#define BUFFER_LAYERS 12
class OITRenderer
{
public:
	OITRenderer(GLRenderer* renderer, ResourceLoader* loader);
	~OITRenderer();

	void ClearBuffers();

	void BeginRendering();

	void DrawMesh(MeshPtr mesh, Camera* cam);

	void Resolve();

	ShaderPtr GetResolveShader();

	TexturePtr GetResolvedBuffer();
private:
	GLRenderer* m_Renderer;
	ivec2 m_Size;

	/*
		Normally would need less textures for OIT, but since I'm intending to do actual lighting 
		and cone tracing with it, need to effectively store multi layer GBuffer.
	*/
	TexturePtr m_DiffuseAlpha;
	TexturePtr m_NormalDepth;
	TexturePtr m_RoughnessMetal;
	TexturePtr m_LayerCount;

	TexturePtr m_ResolvedBuffer;

	ShaderPtr m_ClearBuffersCS;
	ShaderPtr m_RenderBuffersVS;
	ShaderPtr m_RenderBuffersPS;
	ShaderPtr m_ResolveBufferCS;
};
#endif