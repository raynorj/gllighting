#include "..\..\include\app\OITRenderer.h"

OITRenderer::OITRenderer(GLRenderer* renderer, ResourceLoader* loader)
{
	m_Renderer = renderer;
	RendererOptions opts = m_Renderer->GetOptions();

	u32 size = 4 * opts.WindowWidth * opts.WindowHeight;

	m_Size.x = opts.WindowWidth;
	m_Size.y = opts.WindowHeight;

	f32* dat = new f32[size];
	std::fill_n(dat, size, 0.0f);

	size = 2 * opts.WindowWidth * opts.WindowHeight;
	f32* dat2 = new f32[size];	
	std::fill_n(dat2, size, 0.0f);

	m_DiffuseAlpha = std::make_shared<Texture>();
	m_DiffuseAlpha->Create(TT_2D_ARRAY, TF_RGBA16F, IF_RGBA, TD_FLOAT, opts.WindowWidth, opts.WindowHeight, BUFFER_LAYERS);


	m_NormalDepth = std::make_shared<Texture>();
	m_NormalDepth->Create(TT_2D_ARRAY, TF_RGBA32F, IF_RGBA, TD_FLOAT, opts.WindowWidth, opts.WindowHeight, BUFFER_LAYERS);

	m_RoughnessMetal = std::make_shared<Texture>();
	m_RoughnessMetal->Create(TT_2D_ARRAY, TF_RG16F, IF_RG, TD_FLOAT, opts.WindowWidth, opts.WindowHeight, BUFFER_LAYERS);

	m_ResolvedBuffer = std::make_shared<Texture>();
	m_ResolvedBuffer->Create(TT_2D, TF_RGBA16F, IF_RGBA, TD_FLOAT, opts.WindowWidth, opts.WindowHeight);
	m_ResolvedBuffer->SetData(dat);
	for (u32 i = 0; i < BUFFER_LAYERS; i++)
	{
		m_DiffuseAlpha->SetFaceData(i, dat);
		m_NormalDepth->SetFaceData(i, dat);
		m_RoughnessMetal->SetFaceData(i, dat2);
	}

	//std::fill_n(dat, size, 0.0f);
	m_LayerCount = std::make_shared<Texture>();
	m_LayerCount->Create(TT_2D, TF_R32UI, IF_RI, TD_UINT, opts.WindowWidth, opts.WindowHeight);

	size = opts.WindowWidth * opts.WindowHeight;
	delete dat;
	delete dat2;

	u32* dat3  = new u32[size];
	std::fill_n(dat3, size, (u32)0);

	m_LayerCount->SetData(dat3);

	delete dat3;

	m_ClearBuffersCS = loader->LoadShader("shaders/Clear_A_Buffer.cs");
	m_RenderBuffersVS = loader->LoadShader("shaders/G_Buffer.vs");
	m_RenderBuffersPS = loader->LoadShader("shaders/A_Buffer.ps");
	m_ResolveBufferCS = loader->LoadShader("shaders/A_Buffer_Resolve.cs");
	
}

void OITRenderer::ClearBuffers()
{
	m_Renderer->SetActiveShader(m_ClearBuffersCS);

	m_Renderer->BindImage(0, m_DiffuseAlpha, BA_WRITE);
	m_Renderer->BindImage(1, m_NormalDepth, BA_WRITE);
	m_Renderer->BindImage(2, m_RoughnessMetal, BA_WRITE);
	m_Renderer->BindImage(3, m_LayerCount, BA_WRITE);
	m_Renderer->BindImage(4, m_ResolvedBuffer, BA_WRITE);

	m_ClearBuffersCS->Dispatch(m_Size.x / 8, m_Size.y / 4, 1);
	glMemoryBarrier(MB_IMAGE);

}

void OITRenderer::BeginRendering()
{
	ClearBuffers();
	glViewport(0, 0, m_Size.x, m_Size.y);
	glColorMask(false, false, false, false);
	m_Renderer->SetActiveShader(m_RenderBuffersVS);
	m_Renderer->SetActiveShader(m_RenderBuffersPS);
	m_Renderer->SetDepthWrite(false);
	m_Renderer->SetDepthFunc(CF_LEQUAL);
	m_Renderer->Disable(OP_CULL_FACE);

	m_ClearBuffersCS->SetUniform(0, BUFFER_LAYERS);
	m_ResolveBufferCS->SetUniform(0, BUFFER_LAYERS);
	m_RenderBuffersPS->SetUniform(0, BUFFER_LAYERS);
}

void OITRenderer::DrawMesh(MeshPtr mesh, Camera* cam)
{

	/*
		Since this is only for OIT, assume transparent meshes.
		For the sake of simplicity, the demo only renders one opaque and one transparent mesh
		Sponza Scene, and a teapot
	
	*/

	mat4 mvp = cam->GetProjectionMatrix() * cam->GetViewMatrix() * mesh->GetModelMatrix();
	m_RenderBuffersVS->SetUniform(4, mesh->GetModelMatrix());
	m_RenderBuffersVS->SetUniform(2, mvp);


	for (u32 i = 0; i < mesh->GetPartCount(); i++)
	{
		VertexArray* vao = mesh->GetPart(i)->GetVertexArray();
		Material* mat = mesh->GetPart(i)->GetMaterial();

		if (mat->GetUseDiffuseMap())
		{
			TexturePtr diffuse = mat->GetDiffuseMap();
			//m_Renderer->Bind(0, diffuse);
		}
		else
		{

		}
		

		TexturePtr normal = mat->GetNormalMap();
		//m_Renderer->Bind(1, normal);

		TexturePtr opacity = mat->GetOpacityMap();
		//m_Renderer->Bind(2, opacity);

		TexturePtr metalness = mat->GetMetalnessMap();
		//m_Renderer->Bind(3, metalness);

		TexturePtr roughness = mat->GetRoughnessMap();
		//m_Renderer->Bind(4, roughness);

		m_Renderer->Bind(vao);
		m_Renderer->DrawElements(vao->GetSize(), PT_TRI);
		glMemoryBarrier(MB_IMAGE);
	}
}

void OITRenderer::Resolve()
{
	m_Renderer->SetActiveShader(m_ResolveBufferCS);

	m_Renderer->BindImage(0, m_DiffuseAlpha, BA_READ);
	m_Renderer->BindImage(1, m_NormalDepth, BA_READ);
	m_Renderer->BindImage(2, m_RoughnessMetal, BA_READ);
	m_Renderer->BindImage(3, m_LayerCount, BA_READ);

	m_Renderer->BindImage(4, m_ResolvedBuffer, BA_WRITE);

	s32 tile = 1;
	vec2 tiles = m_Size / tile;
	for (s32 i = 0; i < tile; i++)
	{
		for (s32 j = 0; j < tile; j++)
		{
			m_ResolveBufferCS->SetUniform(19, vec2(i, j) * tiles);
			m_ResolveBufferCS->Dispatch(tiles.x / 8, tiles.y / 8, 1);
			glMemoryBarrier(MB_IMAGE);
		}
	}
		
	
	glMemoryBarrier(MB_IMAGE);

	m_Renderer->SetDepthWrite(true);
	m_Renderer->Enable(OP_CULL_FACE);
	glColorMask(true, true, true, true);
	//m_Renderer->Disable(OP_BLEND);
}

ShaderPtr OITRenderer::GetResolveShader()
{
	return m_ResolveBufferCS;
}

TexturePtr OITRenderer::GetResolvedBuffer()
{
	return m_ResolvedBuffer;
}
