#include "PBRApp.h"

#define DEMO 0
PBRApp::PBRApp()
{
	/*
		TODO: massive architecture overhaul. This is unsustainable.
		Investigate use of render graph and actual abstraction instead of wrapper
	*/
}
void PBRApp::Init(string root_dir)
{
	GLApp::Init(DEVELOPMENT_DIRECTORY);

	ResourceLoader* loader = GetLoader();
	GLRenderer* renderer = GetRenderer();
	RendererOptions opts = renderer->GetOptions();
	Camera* cam = GetCamera();

	m_DeferredRenderer = new DeferredRenderer(GetRenderer(), GetLoader());
	m_VolumeRenderer = new VolumeRenderer(renderer, loader, ivec3(256));

	f32 size = 1900.0;
	m_VolumeRenderer->SetVolumeExtents(vec4(-size, size, -size, size));
	m_VolumeRenderer->SetNearPlane(0.1f);
	m_VolumeRenderer->SetFarPlane(2 * size);
	m_VolumeRenderer->SetVolumePosition(vec3(0.0, 0.0, 0.0));

	m_OIT = new OITRenderer(GetRenderer(), GetLoader());

	TexturePtr checker = loader->LoadTexture("assets/cube/default.png");
	m_Cube = loader->LoadMesh("assets/cube/cube.obj");
	m_Sponza = loader->LoadMesh("assets/crytek-sponza/sponzaPBR.obj");
	//sponza = cube;

	
	//cam_move_speed = 0.02f;
	m_Teapot = loader->LoadMesh("assets/teapot/teapot.obj");
	Material* iron = loader->LoadMaterial("assets/materials/rustediron.json");


	for (u32 i = 0; i < m_Teapot->GetPartCount(); i++)
	{
		m_Teapot->GetPart(i)->SetMaterial(iron);
	}
	f32 tan_fov = tan(m_Camera->GetFOV() / 2.0f);
	vec2 half_near = vec2(tan_fov * m_Camera->GetAspectRatio(), tan_fov);

	vec3 start_pos = cam->GetPosition();
#if DEMO
	cam->SetPosition(vec3(-12.0f, 388.65f, -119.84));
	cam->SetDirection(vec3(-0.97, -0.17, 0.17));
#else
	cam->SetPosition(vec3(-10.0f, 15.0f, 0.0f));
	cam->SetDirection(vec3(0.0f, -1.0f, 0.0f));
#endif
	m_Scene = new Entity();
	m_Sponza->SetPosition(vec3(0.0f, 0.0f, 0.0f));
	m_Sponza->SetRotation(vec3(0.0f));
	m_Sponza->SetScale(vec3(1.0f));

	m_Teapot->SetPosition(vec3(0.0f, 0.0f, 0.0f));
	m_Teapot->SetRotation(vec3(0.0f));
	m_Teapot->SetScale(vec3(2.0f));


	//Camera->SetDirection(glm::normalize(m_Camera->GetPosition() - m_Scene->GetPosition()));

	m_Camera->SetFarPlane(5000.0f);

	renderer->SetDepthWrite(true);
	renderer->Enable(OP_DEPTH_TEST);
	renderer->Enable(OP_CULL_FACE);

#if defined(CAM_LIGHT_POINT)
	m_CamLight = new PointLight();
	m_CamLight->SetRadius(1000.0f);
	m_CamLight->SetIntensity(15000);
#elif defined(CAM_LIGHT_SPOT)
	m_CamLight = new SpotLight();
	m_CamLight->SetRadius(1000.0f);
	m_CamLight->SetIntensity(15000);
	m_CamLight->SetConeAngle(30.0f);

	m_CamLight->SetPosition(vec3(-640.0f, 290.0f, 81.0f));
	m_CamLight->SetDirection(vec3(-1.0, -0.3, 0.1));
	
#endif

	m_DeferredRenderer->AddLight(m_CamLight);

	// Create a tweak bar
	TwBar* bar = GetUIBar();
	TwAddVarRW(bar, "Lock Cam Light", TW_TYPE_BOOL8, &lock_cam_light, "group=Visualization");
	TwAddVarRW(bar, "Conservative Raster", TW_TYPE_BOOL8, &conservative_raster, "group=Visualization");

	TwEnumVal debug_ev[] = {
			{ SCENE, "Scene" },
			{ G_BUFFER, "G-buffer" },
			{ OIT, "OIT" },
			{ AO, "AO" },
			{ DIFFUSE, "Diffuse" },
			{ SPECULAR, "Specular" },
			{ EMITTANCE, "Emittance" },
			{ OPACITY, "Opacity" }
	};
	TwType debug_type = TwDefineEnum("DebugEnum", debug_ev, 8);

	TwAddVarRW(bar, "Debug View", debug_type, &debug_view, "");

	TwEnumVal dir_ev[] = 
	{
		{ NEG_X, "Neg X" },
		{ NEG_Y, "Neg Y" },
		{ NEG_Z, "Neg Z" },
		{ POS_X, "Pos X" },
		{ POS_Y, "Pos Y" },
		{ POS_Z, "Pos Z" },
	};

	TwType vis_dir_type = TwDefineEnum("DirEnum", dir_ev, 6);

	TwAddVarRW(bar, "Vis. Dir.", vis_dir_type, &vis_dir, "group=Visualization");

	TwAddVarRW(bar, "Voxel Mip", TW_TYPE_INT32, &voxel_mip, "group=Visualization min=0 max=5");

	TwAddVarRW(bar, "Cam Light Color", TW_TYPE_COLOR3F, &(cam_light_color), "group=Light");
	TwAddVarRW(bar, "Cam Light Intensity", TW_TYPE_FLOAT, &(cam_light_intensity), "group=Light");
	TwAddVarRW(bar, "Cam Light Radius", TW_TYPE_FLOAT, &(cam_light_radius), "group=Light");
}

void PBRApp::Destroy()
{
	GLApp::Destroy();
}

void PBRApp::Run()
{
	GLApp::Run();
}

void PBRApp::RenderFrame()
{
	// update camera matrices and camera light
	m_CameraMatrices.M = m_Scene->GetModelMatrix();
	m_CameraMatrices.V = m_Camera->GetViewMatrix();
	m_CameraMatrices.P = m_Camera->GetProjectionMatrix();
	m_CameraMatrices.MV = m_CameraMatrices.V * m_CameraMatrices.M;
	m_CameraMatrices.MVP = m_CameraMatrices.P * m_CameraMatrices.MV;


	mat4 inv_v = glm::inverse(m_CameraMatrices.MV);
	mat4 inv_p = glm::inverse(m_CameraMatrices.P);
	mat4 inv_pv = glm::inverse(m_CameraMatrices.P * m_CameraMatrices.V);

	ShaderPtr gvs = m_DeferredRenderer->GetGBufferVS();
	ShaderPtr gps = m_DeferredRenderer->GetGBufferPS();
	ShaderPtr plight_ps = m_DeferredRenderer->GetPointLightingPS();

	ShaderPtr slight_ps = m_DeferredRenderer->GetSpotLightingPS();

	if (m_CamLight->GetPosition() != m_Camera->GetPosition() 
		|| m_CamLight->GetDirection() != m_Camera->GetDirection()
		|| m_CamLight->GetColor() != cam_light_color
		|| m_CamLight->GetIntensity() != cam_light_intensity
		|| m_CamLight->GetRadius() != cam_light_radius)
	{
		m_VolumeRenderer->SetEmittanceDirty(true);
	}
#if DEMO
	lock_cam_light = true;
#endif
	if (!lock_cam_light)
	{
		
		m_CamLight->SetPosition(cam_pos);
#if defined(CAM_LIGHT_SPOT)
		m_CamLight->SetDirection(cam_dir);
#endif
	}
#if DEMO
	lock_cam_light = false;
#endif
	m_VolumeRenderer->SetConservativeRaster(true);

	m_CamLight->SetColor(cam_light_color);
	m_CamLight->SetIntensity(cam_light_intensity);
	m_CamLight->SetRadius(cam_light_radius);

	m_DeferredRenderer->BeginFrame();
	m_Renderer->SetPolygonMode(PF_FRONT_AND_BACK, poly_mode);

	MeshPtr mesh = m_Sponza;


	// GBuffer pass
	{
		rmt_ScopedOpenGLSample(GBuffer);
		m_DeferredRenderer->BeginRenderGBuffer();
	
		m_DeferredRenderer->DrawMesh(m_Sponza, m_Camera);		m_Teapot->SetScale(vec3(2.0f));

		// do transparent pass
		{
			rmt_ScopedOpenGLSample(OITRecord)
			m_OIT->BeginRendering();
			m_OIT->DrawMesh(m_Teapot, m_Camera);	
		}

		m_DeferredRenderer->EndRenderGBuffer();
	}

	
	
	
	// shadow pass
	// could possibly roll opacity voxelization into this, if I add a directional light later
	// with most engines only needing one directional light ever, might be faster.
	//if(!lock_cam_light)
	{
		rmt_ScopedOpenGLSample(Shadows);
		m_DeferredRenderer->BeginRenderSpotShadows(m_CamLight, m_Camera);
		m_DeferredRenderer->RenderShadowMesh(m_Sponza);
		//m_DeferredRenderer->RenderShadowMesh(m_Teapot);
		m_DeferredRenderer->EndRenderSpotShadows();
	}
	

	// voxelization pass
	//if(frames % 3 == 0)
	//if(!lock_cam_light && frames % 1 == 0)
	{
		rmt_ScopedOpenGLSample(Voxelization, 0);

		m_VolumeRenderer->ClearVolume();
		m_Renderer->SetCullFace(PF_BACK);

		m_VolumeRenderer->BeginVoxelize();
		{
			rmt_ScopedOpenGLSample(VoxelizeOpacity);
			m_Renderer->Enable(OP_CULL_FACE);
			m_Renderer->Disable(OP_CULL_FACE);
			m_VolumeRenderer->VoxelizeMeshOpacity(mesh);
			//m_VolumeRenderer->VoxelizeMeshOpacity(m_Teapot, 0.25f);
			
		}
		m_Renderer->Bind(6, m_DeferredRenderer->GetShadowmap());
		{
			rmt_ScopedOpenGLSample(VoxelizeEmittance);
			
			m_Renderer->Enable(OP_CULL_FACE);
			m_VolumeRenderer->VoxelizeMeshEmittance(mesh, m_CamLight);
			//m_VolumeRenderer->VoxelizeMeshEmittance(m_Teapot, m_CamLight);
		}
		{
			rmt_ScopedOpenGLSample(Mipmapping);
			m_VolumeRenderer->EndVoxelize();
		}
	}
#if DEMO
	lock_cam_light = true;
#endif
	m_VolumeRenderer->SetOpacityDirty(true);
	m_VolumeRenderer->SetEmittanceDirty(true);
	// light pass
	m_DeferredRenderer->BeginRenderPointLights();
	/*for (PointLight* l : m_DeferredRenderer->GetPointLights())
	{

		plight_ps->SetUniform(5, m_Camera->GetPosition());
		plight_ps->SetUniform(6, m_Camera->GetDirection());
		plight_ps->SetUniform(9, inv_pv);

		plight_ps->SetUniform(10, l->GetPosition());
		plight_ps->SetUniform(11, l->GetColor());
		plight_ps->SetUniform(12, l->GetIntensity());
		plight_ps->SetUniform(13, l->GetRadius());
		m_Renderer->RenderFSQuad();
	}*/


	

	{
		rmt_ScopedOpenGLSample(SpotLight);

		m_DeferredRenderer->BeginRenderSpotLights();
		for (SpotLight* l : m_DeferredRenderer->GetSpotLights())
		{

			slight_ps->SetUniform(5, m_Camera->GetPosition());
			slight_ps->SetUniform(6, m_Camera->GetDirection());
			slight_ps->SetUniform(7, inverse(m_Camera->GetViewMatrix()));
			slight_ps->SetUniform(9, inv_pv);

			slight_ps->SetUniform(10, l->GetPosition());
			slight_ps->SetUniform(11, l->GetDirection());
			slight_ps->SetUniform(12, l->GetColor());
			slight_ps->SetUniform(13, l->GetIntensity());
			slight_ps->SetUniform(14, l->GetRadius());
			slight_ps->SetUniform(15, l->GetConeAngle());
			slight_ps->SetUniform(15, l->GetConeAngle());
			slight_ps->SetUniform(16, l->GetCamera()->GetViewMatrix());
			slight_ps->SetUniform(17, l->GetCamera()->GetProjectionMatrix());

			slight_ps->SetUniform(18, m_Camera->GetProjectionMatrix());
			
			m_Renderer->RenderFSQuad();
		}
		m_DeferredRenderer->EndRenderLights();
	}
	
	
	ShaderPtr res = m_OIT->GetResolveShader();

	/*
		Hardcode 0th light, since only one light is added for this project
	*/
	SpotLight* l = m_DeferredRenderer->GetSpotLights()[0];

	res->SetUniform(5, m_Camera->GetPosition());
	res->SetUniform(6, m_Camera->GetDirection());

	//light_ps->SetUniform(7, inv_v);
	//light_ps->SetUniform(8, inv_p);
	res->SetUniform(9, inv_pv);
	res->SetUniform(7, inverse(m_Camera->GetViewMatrix()));
	res->SetUniform(10, l->GetPosition());
	res->SetUniform(11, l->GetDirection());
	res->SetUniform(12, l->GetColor());
	res->SetUniform(13, l->GetIntensity());
	res->SetUniform(14, l->GetRadius());
	res->SetUniform(15, l->GetConeAngle());

	res->SetUniform(4, m_VolumeRenderer->GetMipCount() - 1);
	res->SetUniform(1, m_VolumeRenderer->GetVoxelExtents());
	res->SetUniform(3, vec2(1280, 720));
	res->SetUniform(0, 12);
	res->SetUniform(2, m_VolumeRenderer->GetVoxelResolution());
	switch (debug_view)
	{
	case SCENE:
		{
			rmt_ScopedOpenGLSample(GatherIndirect);
			m_VolumeRenderer->GatherIndirect(m_DeferredRenderer->GetGBuffer(), 2, m_Camera);
		}
		// apply
		{
			rmt_ScopedOpenGLSample(ApplyLighting)
			m_Renderer->BindImage(2, m_VolumeRenderer->GetResolvedTrace(), BA_READ);
			m_DeferredRenderer->BeginApplyLighting();
			m_DeferredRenderer->EndApplyLighting();
		}


		// get brightness
		{
			rmt_ScopedOpenGLSample(PostProcess)
			m_DeferredRenderer->BeginLumaConvert();

			// tonemap
			m_DeferredRenderer->BeginTonemap();
			m_DeferredRenderer->EndRenderPost();
		}
		m_DeferredRenderer->RenderFinalScene();

		// resolve and cone trace for OIT
		m_Renderer->BindImage(5, m_DeferredRenderer->GetFinalScene(), BA_READ);
		m_Renderer->Bind(0, m_DeferredRenderer->GetShadowmap());
		{
			rmt_ScopedOpenGLSample(OITResolve);
			m_VolumeRenderer->BindVolumes();
			m_OIT->Resolve();
		}
		m_Renderer->Bind(0, m_OIT->GetResolvedBuffer());
		break;
	case G_BUFFER:
		m_DeferredRenderer->RenderDebugView();
		break;
	case OIT:
		m_Renderer->Bind(0, m_OIT->GetResolvedBuffer());
		break;
	case AO:
		{
			rmt_ScopedOpenGLSample(VoxelAO);
			m_VolumeRenderer->TraceAO(m_DeferredRenderer->GetGBuffer(), 1, m_Camera);
			m_Renderer->Bind(0, m_VolumeRenderer->GetResolvedAO());
		}
		break;
	case DIFFUSE:
		{
			rmt_ScopedOpenGLSample(VoxelDiffuse);
			m_VolumeRenderer->TraceDiffuseAO(m_DeferredRenderer->GetGBuffer(), 1, m_Camera);
			m_Renderer->Bind(0, m_VolumeRenderer->GetResolvedDiffuse());
		}
		break;
	case SPECULAR:
		{
			rmt_ScopedOpenGLSample(SpecularTrace);
			m_VolumeRenderer->TraceSpecular(m_DeferredRenderer->GetGBuffer(), 1, m_Camera);
			m_Renderer->Bind(0, m_VolumeRenderer->GetResolvedSpecular());
		}
		break;
	case OPACITY:
		{
			rmt_ScopedOpenGLSample(AOTrace);
			m_VolumeRenderer->RaytraceOpacity(m_Camera, vis_dir, voxel_mip);
			m_Renderer->Bind(0, m_VolumeRenderer->GetResolvedTrace());
		}
		break;
	case EMITTANCE:
		{
			rmt_ScopedOpenGLSample(EmittanceTrace);
			m_VolumeRenderer->RaytraceEmittance(m_Camera, vis_dir, voxel_mip);
			m_Renderer->Bind(0, m_VolumeRenderer->GetResolvedTrace());
		}
		break;
	}
	//m_Renderer->Bind(0, m_OIT->GetResolvedBuffer());

	if (debug_view != G_BUFFER)
	{
		m_Renderer->SetActiveShader(m_Renderer->GetQuadPS());
	}
	
	//m_Renderer->Bind(0, m_VolumeRenderer->GetResolvedTexture());
	m_Renderer->RenderFSQuad();
}
