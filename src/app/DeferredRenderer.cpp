#include "DeferredRenderer.h"

DeferredRenderer::DeferredRenderer(GLRenderer* renderer, ResourceLoader* loader)
{
	RendererOptions opts = renderer->GetOptions();
	m_Renderer = renderer;
	m_Loader = loader;

	/*
		Create GBuffer
	*/
	// Create albedo texture, with roughness in alpha
	// Create normal texture, with metallic in alpha
	// Create depth buffer

#if REVERSE_Z
	vec4 depth_clear = vec4(0.0f);
#else
	vec4 depth_clear = vec4(1.0f);
#endif
	m_GBuffer.push_back(renderer->CreateRenderTargetTexture(opts.WindowSize, TF_RGBA32F));
	m_GBuffer.push_back(renderer->CreateRenderTargetTexture(opts.WindowSize, TF_RGBA32F));
	m_GBuffer.push_back(renderer->CreateRenderTargetTexture(opts.WindowSize, TF_DEPTH_32, depth_clear));
	
	std::for_each(m_GBuffer.begin(), m_GBuffer.end(), [](TexturePtr t)
	{
		t->SetWrapS(WM_CLAMP);
		t->SetWrapT(WM_CLAMP);
		t->SetMagFilter(FM_LINEAR);
		t->SetMinFilter(FM_LINEAR);
	});

	m_Size = opts.WindowSize;

	m_GBufferFBO = renderer->CreateFramebuffer(opts.WindowSize, m_GBuffer);

	// Create light accumulation buffer
	m_LightBuffer = m_Renderer->CreateRenderTarget2D(opts.WindowSize, TF_RGBA32F);

	// luminance buffer for auto exposure
	m_LumaBuffer = m_Renderer->CreateRenderTargetTexture(opts.WindowSize, TF_RGBA32F);


	// final HDR buffer
	m_HDRLitScene = m_Renderer->CreateRenderTargetTexture(opts.WindowSize, TF_RGBA32F);

	m_FinalScene = m_Renderer->CreateRenderTargetTexture(opts.WindowSize, TF_RGBA32F);

	// Set up shadow maps
	m_SpotShadowMap = m_Renderer->CreateRenderTarget2D(glm::ivec2(2048), TF_DEPTH_32, depth_clear);
	TexturePtr t = m_SpotShadowMap->GetTexturePtr();
	t->SetWrapS(WM_CLAMP);
	t->SetWrapT(WM_CLAMP);
	t->SetMagFilter(FM_NONE);
	t->SetMinFilter(FM_NONE);

	m_SpotShadowVS = loader->LoadShader("shaders/Opaque_Shadowmap.vs");
	m_SpotShadowPS = loader->LoadShader("shaders/Opaque_Shadowmap.ps");
	m_ShadowDebug = loader->LoadShader("shaders/Shadowmap_Debug.ps");

	// Set up Deferred shaders
	m_GBufferVS = loader->LoadShader("shaders/G_Buffer.vs");
	m_GBufferPS = loader->LoadShader("shaders/G_Buffer.ps");
	m_GBufferDebug = loader->LoadShader("shaders/G_Buffer_Debug.ps");

	m_PointLighting = loader->LoadShader("shaders/Deferred_Point_Light.ps");
	m_SpotLighting = loader->LoadShader("shaders/Deferred_Spot_Light.ps");
	m_ApplyLighting = loader->LoadShader("shaders/Apply_Lighting.cs");

	// post process 
	m_LumaConvert = loader->LoadShader("shaders/Luma_Convert.cs");
	m_Tonemap = loader->LoadShader("shaders/ACES_Tonemap.cs");

	// Load texture that replaces invalid or missing textures
	m_CheckerTexture = loader->LoadTexture("assets/cube/default.png");

}

void DeferredRenderer::AddLight(PointLight* light)
{
	m_PointLights.push_back(light);
}

void DeferredRenderer::AddLight(SpotLight* light)
{
	m_SpotLights.push_back(light);
}


void DeferredRenderer::DrawMesh(MeshPtr mesh, Camera* cam)
{
	mat4 mvp = cam->GetProjectionMatrix() * cam->GetViewMatrix() * mesh->GetModelMatrix();
	m_GBufferVS->SetUniform(4, mesh->GetModelMatrix());
	m_GBufferVS->SetUniform(2, mvp);

	for (u32 i = 0; i < mesh->GetPartCount(); i++)
	{
		VertexArray* vao = mesh->GetPart(i)->GetVertexArray();
		Material* mat = mesh->GetPart(i)->GetMaterial();

		TexturePtr diffuse = mat->GetDiffuseMap();
		if (diffuse == nullptr)
		{
			diffuse = m_CheckerTexture;
		}

		m_Renderer->Bind(0, diffuse);

		TexturePtr normal = mat->GetNormalMap();
		if (normal == nullptr)
		{
			normal = m_CheckerTexture;
		}

		m_Renderer->Bind(1, normal);

		TexturePtr opacity = mat->GetOpacityMap();
		if (opacity == nullptr)
		{
			opacity = m_CheckerTexture;
		}

		m_Renderer->Bind(2, opacity);

		TexturePtr metalness = mat->GetMetalnessMap();
		if (metalness == nullptr)
		{
			metalness = m_CheckerTexture;
		}

		m_Renderer->Bind(3, metalness);

		TexturePtr roughness = mat->GetRoughnessMap();
		if (roughness == nullptr)
		{
			roughness = m_CheckerTexture;
		}

		m_Renderer->Bind(4, roughness);
		m_Renderer->Bind(vao);
		m_Renderer->DrawElements(vao->GetSize(), PT_TRI);
	}
}

void DeferredRenderer::BeginFrame()
{
	m_Renderer->SetDepthWrite(true);
	m_Renderer->Enable(OP_CULL_FACE);
	m_Renderer->Enable(OP_DEPTH_TEST);
	m_LightBuffer->GetFBO()->BeginDraw();
}

void DeferredRenderer::EndFrame()
{
	
}

void DeferredRenderer::BeginRenderGBuffer()
{
	m_Renderer->SetActiveShader(m_GBufferVS);
	m_Renderer->SetActiveShader(m_GBufferPS);
	m_Renderer->SetActiveFramebuffer(m_GBufferFBO);
	m_Renderer->SetDepthWrite(true);
	m_Renderer->Enable(OP_DEPTH_TEST);
	m_Renderer->SetCullFace(PF_BACK);
	m_GBufferFBO->BeginDraw();
}

void DeferredRenderer::EndRenderGBuffer()
{
	m_Renderer->SetActiveFramebuffer();
	//m_Renderer->Clear(BB_COLOR);
}

void DeferredRenderer::BeginRenderPointLights()
{
	m_Renderer->SetActiveFramebuffer(m_LightBuffer->GetFBO());
	m_Renderer->SetActiveShader(m_PointLighting);

	for (u32 i = 0; i < m_GBuffer.size(); i++)
	{
		m_Renderer->Bind(i, m_GBuffer[i]);
	}

}

void DeferredRenderer::BeginRenderSpotLights()
{
	m_Renderer->SetActiveFramebuffer(m_LightBuffer->GetFBO());
	m_Renderer->SetActiveShader(m_SpotLighting);

	for (u32 i = 0; i < m_GBuffer.size(); i++)
	{
		m_Renderer->Bind(i, m_GBuffer[i]);
	}

	m_Renderer->Bind(3, m_SpotShadowMap->GetTexturePtr());
}

void DeferredRenderer::EndRenderLights()
{
	m_Renderer->SetActiveFramebuffer();
	m_Renderer->Bind(0, m_LightBuffer->GetTexturePtr());
}

void DeferredRenderer::BeginRenderSpotShadows(SpotLight* light, Camera* cam)
{
	m_Renderer->SetActiveFramebuffer(m_SpotShadowMap->GetFBO());
	glColorMask(false, false, false, false);
	m_SpotShadowMap->GetFBO()->BeginDraw();
	
	Camera* c = light->GetCamera();
	//c = cam;
	c->Update();

	mat4 v = c->GetViewMatrix();
	mat4 p = c->GetProjectionMatrix();

	m_Renderer->SetActiveShader(m_SpotShadowVS);
	m_Renderer->SetActiveShader(m_SpotShadowPS);
	m_SpotShadowVS->SetUniform(1, v);
	m_SpotShadowVS->SetUniform(2, p);

	glViewport(0, 0, 2048, 2048);
	m_Renderer->SetDepthWrite(true);
	m_Renderer->Enable(OP_DEPTH_TEST);
	//m_Renderer->Enable(OP_CULL_FACE);
	glClear(GL_DEPTH_BUFFER_BIT);
	//m_Renderer->SetCullFace(PF_FRONT);

}

void DeferredRenderer::RenderShadowMesh(MeshPtr mesh)
{
	m_SpotShadowVS->SetUniform(3, mesh->GetModelMatrix());
	for (u32 i = 0; i < mesh->GetPartCount(); i++)
	{
		Material* mat = mesh->GetPart(i)->GetMaterial();
		TexturePtr diffuse = mat->GetDiffuseMap();
		if (diffuse == nullptr)
		{
			diffuse = m_CheckerTexture;
		}

		m_Renderer->Bind(0, diffuse);
		VertexArray* vao = mesh->GetPart(i)->GetVertexArray();
		m_Renderer->Bind(vao);
		m_Renderer->DrawElements(vao->GetSize(), PT_TRI);
	}
}

void DeferredRenderer::EndRenderSpotShadows()
{
	m_Renderer->SetActiveFramebuffer();
	glColorMask(true, true, true, true);
	ivec2 size = m_Renderer->GetOptions().WindowSize;
	glViewport(0, 0, size.x, size.y);
	m_Renderer->SetCullFace(PF_BACK);
}

void DeferredRenderer::RenderDebugView()
{
	m_Renderer->Disable(OP_DEPTH_TEST);
	m_Renderer->SetActiveShader(m_GBufferDebug);
	for (u32 i = 0; i < m_GBuffer.size(); i++)
	{
		m_Renderer->Bind(i, m_GBuffer[i]);
	}//*/

	//m_Renderer->SetActiveShader(m_ShadowDebug);
	//m_Renderer->Bind(2, m_SpotShadowMap->GetTexturePtr());
	//m_Renderer->Bind(0, m_GBuffer.at(2));
}

void DeferredRenderer::RenderPost()
{

}

void DeferredRenderer::BeginApplyLighting()
{
	m_Renderer->SetActiveShader(m_ApplyLighting);

	m_Renderer->BindImage(0, m_GBuffer.at(0), BA_READ);
	m_Renderer->BindImage(1, m_LightBuffer->GetTexturePtr(), BA_READ);
	m_Renderer->BindImage(3, m_HDRLitScene, BA_WRITE);
	m_ApplyLighting->Dispatch(m_Size.x / 8, m_Size.y / 4, 1);
	glMemoryBarrier(MB_IMAGE);
}

void DeferredRenderer::EndApplyLighting()
{
	m_Renderer->Disable(OP_BLEND);
}

void DeferredRenderer::ApplyIndirect()
{
}

void DeferredRenderer::BeginLumaConvert()
{
	m_Renderer->SetActiveShader(m_LumaConvert);

	m_Renderer->BindImage(0, m_HDRLitScene, BA_READ);
	m_Renderer->BindImage(1, m_LumaBuffer, BA_WRITE);
	m_ApplyLighting->Dispatch(m_Size.x / 8, m_Size.y / 4, 1);
	glMemoryBarrier(MB_IMAGE);
}

void DeferredRenderer::BeginTonemap()
{
	m_Renderer->SetActiveShader(m_Tonemap);

	m_Renderer->BindImage(0, m_HDRLitScene, BA_READ);
	m_Renderer->BindImage(1, m_LumaBuffer, BA_READ);
	m_Renderer->BindImage(2, m_FinalScene, BA_WRITE);
	m_ApplyLighting->Dispatch(m_Size.x / 8, m_Size.y / 4, 1);
	glMemoryBarrier(MB_IMAGE);
}

void DeferredRenderer::EndRenderPost()
{
	m_Renderer->SetActiveFramebuffer();
}

void DeferredRenderer::RenderFinalScene()
{
	m_Renderer->Bind(0, m_FinalScene);
}

ShaderPtr DeferredRenderer::GetGBufferVS()
{
	return m_GBufferVS;
}

ShaderPtr DeferredRenderer::GetGBufferPS()
{
	return m_GBufferPS;
}

ShaderPtr DeferredRenderer::GetGBufferDebugPS()
{
	return m_GBufferDebug;
}

ShaderPtr DeferredRenderer::GetPointLightingPS()
{
	return m_PointLighting;
}

ShaderPtr DeferredRenderer::GetSpotLightingPS()
{
	return m_SpotLighting;
}

std::vector<TexturePtr> DeferredRenderer::GetGBuffer()
{
	return m_GBuffer;
}

std::vector<PointLight*> DeferredRenderer::GetPointLights()
{
	return m_PointLights;
}

std::vector<SpotLight*> DeferredRenderer::GetSpotLights()
{
	return m_SpotLights;
}

TexturePtr DeferredRenderer::GetHDRLItScene()
{
	return m_HDRLitScene;
}

TexturePtr DeferredRenderer::GetFinalScene()
{
	return m_FinalScene;
}

TexturePtr DeferredRenderer::GetShadowmap()
{
	return m_SpotShadowMap->GetTexturePtr();
}
