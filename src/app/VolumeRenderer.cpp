#include "VolumeRenderer.h"

VolumeRenderer::VolumeRenderer(GLRenderer* renderer, ResourceLoader* loader, ivec3 size)
{
	m_Renderer = renderer;

	m_ConservativeRaster = false;
	m_OpacityDirty = true;
	m_EmittanceDirty = true;

	m_Size = renderer->GetOptions().WindowSize;

	m_GridSize = size;
	m_GridScale = vec3(1.0) / vec3(size);

	// set up volume
	m_NegXEmittance = m_Renderer->CreateRenderTargetTexture(size, TF_RGBA16F, MIPS);
	m_PosXEmittance = m_Renderer->CreateRenderTargetTexture(size, TF_RGBA16F, MIPS);

	m_NegYEmittance = m_Renderer->CreateRenderTargetTexture(size, TF_RGBA16F, MIPS);
	m_PosYEmittance = m_Renderer->CreateRenderTargetTexture(size, TF_RGBA16F, MIPS);

	m_NegZEmittance = m_Renderer->CreateRenderTargetTexture(size, TF_RGBA16F, MIPS);
	m_PosZEmittance = m_Renderer->CreateRenderTargetTexture(size, TF_RGBA16F, MIPS);

	m_NegXOpacity = m_Renderer->CreateRenderTargetTexture(size, TF_R16F, MIPS);
	m_PosXOpacity = m_Renderer->CreateRenderTargetTexture(size, TF_R16F, MIPS);

	m_NegYOpacity = m_Renderer->CreateRenderTargetTexture(size, TF_R16F, MIPS);
	m_PosYOpacity = m_Renderer->CreateRenderTargetTexture(size, TF_R16F, MIPS);

	m_NegZOpacity = m_Renderer->CreateRenderTargetTexture(size, TF_R16F, MIPS);
	m_PosZOpacity = m_Renderer->CreateRenderTargetTexture(size, TF_R16F, MIPS);

	m_EmittanceVolumes.push_back(m_NegXEmittance);
	m_EmittanceVolumes.push_back(m_NegYEmittance);
	m_EmittanceVolumes.push_back(m_NegZEmittance);
	m_EmittanceVolumes.push_back(m_PosXEmittance);
	m_EmittanceVolumes.push_back(m_PosYEmittance);
	m_EmittanceVolumes.push_back(m_PosZEmittance);

	for (TexturePtr t : m_EmittanceVolumes)
	{
		t->SetWrapR(WM_CLAMP);
		t->SetWrapS(WM_CLAMP);
		t->SetWrapT(WM_CLAMP);

		t->SetMinFilter(FM_LINEAR_MIPMAP_LINEAR);
		t->SetMagFilter(FM_LINEAR);
	}

	m_OpacityVolumes.push_back(m_NegXOpacity);
	m_OpacityVolumes.push_back(m_NegYOpacity);
	m_OpacityVolumes.push_back(m_NegZOpacity);
	m_OpacityVolumes.push_back(m_PosXOpacity);
	m_OpacityVolumes.push_back(m_PosYOpacity);
	m_OpacityVolumes.push_back(m_PosZOpacity);

	for (TexturePtr t : m_OpacityVolumes)
	{
		t->SetWrapR(WM_CLAMP);
		t->SetWrapS(WM_CLAMP);
		t->SetWrapT(WM_CLAMP);

		t->SetMinFilter(FM_LINEAR_MIPMAP_LINEAR);
		t->SetMagFilter(FM_LINEAR);
	}

	// set up shaders
	m_ShellVS = loader->LoadShader("shaders/Ray_March_Shell.vs");
	m_ShellPS = loader->LoadShader("shaders/Ray_March_Shell.ps");

	m_VolumeClear = loader->LoadShader("shaders/Clear_Volume.cs");
	m_BoundsView = loader->LoadShader("shaders/Simple_Debug.ps");
	m_VolumeTraceCS = loader->LoadShader("shaders/Ray_Trace.cs");
	m_EmittanceTraceCS = loader->LoadShader("shaders/Ray_Trace_Emittance.cs");

	m_TraceAO = loader->LoadShader("shaders/AO_Trace.cs");
	m_TraceDiffuse = loader->LoadShader("shaders/Diffuse_AO_Trace.cs");
	m_TraceSpecular = loader->LoadShader("shaders/Specular_Trace.cs");
	m_GatherIndirect = loader->LoadShader("shaders/Gather_Indirect.cs");

	m_VoxelizeVS = loader->LoadShader("shaders/Voxelize.vs");
	m_VoxelizeGS = loader->LoadShader("shaders/Voxelize.gs");
	m_VoxelizeOpacityPS = loader->LoadShader("shaders/Voxelize_Opacity.ps");
	m_VoxelizeEmittancePS = loader->LoadShader("shaders/Voxelize_Emittance.ps");

	m_FilterEmittanceCS = loader->LoadShader("shaders/Filter_Emittance.cs");
	m_DownsampleOpacityCS = loader->LoadShader("shaders/Opacity_Downsample.cs");
	m_DownsampleEmittanceCS = loader->LoadShader("shaders/Emittance_Downsample.cs");

	// .. and cube mesh for traces
	m_Cube = loader->LoadMesh("assets/cube/unit_cube.obj");
	m_Front = m_Renderer->CreateRenderTarget2D(m_Size, TF_RGBA32F);
	m_Back = m_Renderer->CreateRenderTarget2D(m_Size, TF_RGBA32F);
	m_TraceRT = m_Renderer->CreateRenderTarget2D(m_Size, TF_RGBA32F);

	m_AOResolve = m_Renderer->CreateRenderTargetTexture(m_Size, TF_RGBA32F);
	m_DiffuseResolve = m_Renderer->CreateRenderTargetTexture(m_Size, TF_RGBA32F);
	m_SpecularResolve = m_Renderer->CreateRenderTargetTexture(m_Size, TF_RGBA32F);
	m_CheckerTexture = loader->LoadTexture("assets/cube/default.png");

	m_DebugCube = new Entity();
	m_DebugCube->SetScale(vec3(50.0f));
	m_DebugCube->SetPosition(vec3(0.0f));
	m_DebugCube->Update();


	// .. and the cameras for anisotropic voxels

	m_NegXCam = new OrthoCamera();
	m_PosXCam = new OrthoCamera();
	m_NegYCam = new OrthoCamera();
	m_PosYCam = new OrthoCamera();
	m_NegZCam = new OrthoCamera();
	m_PosZCam = new OrthoCamera();

	SetVolumePosition(vec3(0.0));

	m_NegXCam->SetDirection(vec3(-1.0, 0.0, 0.0));
	m_PosXCam->SetDirection(vec3(1.0, 0.0, 0.0));

	m_NegYCam->SetDirection(vec3(0.0, -1.0, 0.0));
	m_PosYCam->SetDirection(vec3(0.0, 1.0, 0.0));

	m_NegZCam->SetDirection(vec3(0.0, 0.0, -1.0));
	m_PosZCam->SetDirection(vec3(0.0, 0.0, 1.0));

	m_VolumeCameras.push_back(m_NegXCam);
	m_VolumeCameras.push_back(m_NegYCam);
	m_VolumeCameras.push_back(m_NegZCam);
	m_VolumeCameras.push_back(m_PosXCam);
	m_VolumeCameras.push_back(m_PosYCam);
	m_VolumeCameras.push_back(m_PosZCam);
}

void VolumeRenderer::BeginVoxelize()
{
	m_Renderer->SetActiveShader(m_VoxelizeVS);
	m_Renderer->SetActiveShader(m_VoxelizeGS);

	glColorMask(false, false, false, false);
	glViewport(0, 0, m_GridSize.x, m_GridSize.y);
	m_Renderer->Disable(OP_DEPTH_TEST);
	m_Renderer->Enable(OP_CULL_FACE);
	m_Renderer->SetCullFace(PF_BACK);
	m_Renderer->SetDepthWrite(false);

	if (m_ConservativeRaster)
	{
		m_Renderer->Enable(OP_CONSERVATIVE_RASTER);
	}

	m_Renderer->SetActiveFramebuffer();
}

void VolumeRenderer::EndVoxelize()
{
	ivec2 size = m_Renderer->GetOptions().WindowSize;
	glColorMask(true, true, true, true);
	glViewport(0, 0, size.x, size.y);
	m_Renderer->Enable(OP_DEPTH_TEST);
	m_Renderer->Enable(OP_CULL_FACE);
	m_Renderer->SetCullFace(PF_BACK);
	m_Renderer->SetDepthWrite(true);
	m_Renderer->Disable(OP_CONSERVATIVE_RASTER);

	m_Renderer->ResetShader(SB_GEOMETRY);
	// filter emittance values to get proper averages
	FilterEmittance();

	// downsample the volumes using compute, which is faster than auto generation of mipmaps
	DownsampleOpacity();
	DownsampleEmittance();


}

void VolumeRenderer::VoxelizeMeshOpacity(MeshPtr mesh, float alpha)
{
	if (!m_OpacityDirty)
	{
		return;
	}

	mat4 m = mesh->GetModelMatrix();
	m_VoxelizeOpacityPS->SetUniform(0, m_GridScale);
	m_VoxelizeOpacityPS->SetUniform(2, alpha);

	m_Renderer->SetActiveShader(m_VoxelizeOpacityPS);

	mat4 v = m_NegZCam->GetViewMatrix();
	mat4 p = m_NegZCam->GetProjectionMatrix();

	for (int j = 0; j < 6; j++)
	{
		mat4 v = m_VolumeCameras[j]->GetViewMatrix();
		mat4 p = m_VolumeCameras[j]->GetProjectionMatrix();

		m_VoxelizeVS->SetUniform(0, m);
		m_VoxelizeVS->SetUniform(1, v);
		m_VoxelizeVS->SetUniform(2, p);

		m_VoxelizeGS->SetUniform(0, m);
		m_VoxelizeGS->SetUniform(1, v);
		m_VoxelizeGS->SetUniform(2, p);

		m_VoxelizeOpacityPS->SetUniform(1, j);
		
		m_Renderer->BindImage(0, m_OpacityVolumes[j], BA_WRITE);

		for (int i = 0; i < mesh->GetPartCount(); i++)
		{
			VertexArray* vao = mesh->GetPart(i)->GetVertexArray();
			m_Renderer->Bind(vao);
			m_Renderer->DrawElements(vao->GetSize(), PT_TRI);
		}
		glMemoryBarrier(MB_IMAGE);
	}
}

void VolumeRenderer::VoxelizeMeshEmittance(MeshPtr mesh)
{
	if (!m_EmittanceDirty)
	{
		return;
	}

	mat4 m = mesh->GetModelMatrix();
	m_VoxelizeEmittancePS->SetUniform(0, m_GridScale);

	m_VoxelizeEmittancePS->SetUniform(3, MIPS);
	m_VoxelizeEmittancePS->SetUniform(4, m_WorldExtents.x);
	m_VoxelizeEmittancePS->SetUniform(5, m_GridSize.x);

	m_Renderer->SetActiveShader(m_VoxelizeEmittancePS);
	
	for (int j = 0; j < 6; j++)
	{
		mat4 v = m_VolumeCameras[j]->GetViewMatrix();
		mat4 p = m_VolumeCameras[j]->GetProjectionMatrix();

		m_VoxelizeVS->SetUniform(0, m);
		m_VoxelizeVS->SetUniform(1, v);
		m_VoxelizeVS->SetUniform(2, p);

		m_VoxelizeGS->SetUniform(0, m);
		m_VoxelizeGS->SetUniform(1, v);
		m_VoxelizeGS->SetUniform(2, p);

		m_VoxelizeEmittancePS->SetUniform(1, j);
		
		m_Renderer->BindImage(0, m_EmittanceVolumes[j], BA_WRITE);
		
		for (int i = 0; i < mesh->GetPartCount(); i++)
		{
			Material* mat = mesh->GetPart(i)->GetMaterial();

			TexturePtr diffuse = mat->GetDiffuseMap();
			if (diffuse == nullptr)
			{
				diffuse = m_CheckerTexture;
			}

			m_Renderer->Bind(0, diffuse);

			TexturePtr normal = mat->GetNormalMap();
			if (normal == nullptr)
			{
				normal = m_CheckerTexture;
			}

			m_Renderer->Bind(1, normal);

			TexturePtr opacity = mat->GetOpacityMap();
			if (opacity == nullptr)
			{
				opacity = m_CheckerTexture;
			}

			m_Renderer->Bind(2, opacity);

			TexturePtr metalness = mat->GetMetalnessMap();
			if (metalness == nullptr)
			{
				metalness = m_CheckerTexture;
			}

			m_Renderer->Bind(3, metalness);

			TexturePtr roughness = mat->GetRoughnessMap();
			if (roughness == nullptr)
			{
				roughness = m_CheckerTexture;
			}

			m_Renderer->Bind(4, roughness);

			VertexArray* vao = mesh->GetPart(i)->GetVertexArray();
			m_Renderer->Bind(vao);
			m_Renderer->DrawElements(vao->GetSize(), PT_TRI);
		}
		glMemoryBarrier(MB_IMAGE);
	}
}

void VolumeRenderer::VoxelizeMeshEmittance(MeshPtr mesh, SpotLight* light)
{
	m_VoxelizeEmittancePS->SetUniform(10, light->GetPosition());
	m_VoxelizeEmittancePS->SetUniform(11, light->GetDirection());
	m_VoxelizeEmittancePS->SetUniform(12, light->GetColor());
	m_VoxelizeEmittancePS->SetUniform(13, light->GetIntensity());
	m_VoxelizeEmittancePS->SetUniform(14, light->GetRadius());
	m_VoxelizeEmittancePS->SetUniform(15, light->GetConeAngle());

	m_VoxelizeEmittancePS->SetUniform(16, light->GetCamera()->GetViewMatrix());
	m_VoxelizeEmittancePS->SetUniform(17, light->GetCamera()->GetProjectionMatrix());

	VoxelizeMeshEmittance(mesh);
}

void VolumeRenderer::FilterEmittance()
{
	rmt_ScopedOpenGLSample(FilterEmittance);

	if (!m_EmittanceDirty)
	{
		return;
	}

	m_Renderer->SetActiveShader(m_FilterEmittanceCS);
	for (s32 i = 0; i < 6; i++)
	{
		m_Renderer->BindImage(0, m_EmittanceVolumes[i], BA_READ);
		m_Renderer->BindImage(1, m_EmittanceVolumes[i], BA_WRITE);
		m_FilterEmittanceCS->Dispatch(m_GridSize.x / 4, m_GridSize.y / 4, m_GridSize.z / 4);
		
	}
	glMemoryBarrier(MB_IMAGE);
}

void VolumeRenderer::DownsampleOpacity()
{
	rmt_ScopedOpenGLSample(OpacityDownsample);

	if (!m_OpacityDirty)
	{
		return;
	}

	m_Renderer->SetActiveShader(m_DownsampleOpacityCS);

	ivec3 src_size = m_GridSize;
	ivec3 dest_size = src_size / 2;
	s32 src_mip = 0, dest_mip = 1, scale = 2;

	for (s32 i = 1; i <= MIPS; i++)
	{
		src_mip = i - 1;
		dest_mip = i;

		for (s32 j = 0; j < 6; j++)
		{
			m_Renderer->Bind(0, m_OpacityVolumes[j]);
			m_Renderer->BindImageLayer(1, m_OpacityVolumes[j], dest_mip, BA_WRITE);
			m_DownsampleOpacityCS->SetUniform(1, j);
			m_DownsampleOpacityCS->SetUniform(0, src_mip);
			m_DownsampleOpacityCS->Dispatch(dest_size.x / 4, dest_size.y / 4, dest_size.z / 4);
		}
		glMemoryBarrier(MB_IMAGE | MB_TEXTURE_FETCH);
		src_size /= 2;
		dest_size /= 2;
	}
}

void VolumeRenderer::DownsampleEmittance()
{
	rmt_ScopedOpenGLSample(EmittanceDownsample);

	if (!m_EmittanceDirty)
	{
		return;
	}

	m_Renderer->SetActiveShader(m_DownsampleEmittanceCS);

	ivec3 src_size = m_GridSize;
	ivec3 dest_size = src_size / 2;
	
	s32 src_mip = 0, dest_mip = 1;
	for (s32 i = 1; i <= MIPS; i++)
	{
		src_mip = i - 1;
		dest_mip = i;

		for (s32 j = 0; j < 6; j++)
		{
			m_Renderer->Bind(0, m_EmittanceVolumes[j]);
			m_Renderer->BindImageLayer(1, m_EmittanceVolumes[j], dest_mip, BA_WRITE);
			m_DownsampleEmittanceCS->SetUniform(1, j);		
			m_DownsampleEmittanceCS->SetUniform(0, src_mip);
			m_DownsampleEmittanceCS->Dispatch(dest_size.x / 4, dest_size.y / 4, dest_size.z / 4);
		}

		glMemoryBarrier(MB_IMAGE | MB_TEXTURE_FETCH);
		src_size /= 2;
		dest_size /= 2;
	}
}

//void VolumeRenderer::VoxelizeMesh(MeshPtr mesh, mat4 model)
//{
	/*

	with imageload/store, we can write output to the voxel grid directly from the fragment shader.
	ortho perspective, with grid position calculated by x, y, and depth, with depth
	being scaled from [-1, 1] to [0, zfar]
	where the far plane and extents of the ortho camera is set based on the 
	physical extents of the grid, plus grid resolution

	*/


//}


void VolumeRenderer::DrawTraceBounds(Camera* cam)
{
	m_Renderer->SetActiveShader(m_ShellVS);
	m_Renderer->SetActiveShader(m_ShellPS);

	mat4 v = cam->GetViewMatrix();
	mat4 p = cam->GetProjectionMatrix();

	mat4 mvp = p * v * m_DebugCube->GetModelMatrix();

	m_ShellVS->SetUniform(2, mvp);
	m_ShellVS->SetUniform(4, m_DebugCube->GetModelMatrix());

	// render front positions
	m_Renderer->SetCullFace(PF_BACK);
	m_Front->GetFBO()->Clear(vec4(0.0f));
	m_Renderer->SetActiveFramebuffer(m_Front->GetFBO());
	for (int i = 0; i < m_Cube->GetPartCount(); i++)
	{
		VertexArray* vao = m_Cube->GetPart(i)->GetVertexArray();
		m_Renderer->Bind(vao);
		m_Renderer->DrawElements(vao->GetSize(), PT_TRI);
	}

	// render back faces
	m_Renderer->SetCullFace(PF_FRONT);
	m_Back->GetFBO()->Clear(vec4(0.0f));
	m_Renderer->SetActiveFramebuffer(m_Back->GetFBO());
	for (int i = 0; i < m_Cube->GetPartCount(); i++)
	{
		VertexArray* vao = m_Cube->GetPart(i)->GetVertexArray();
		m_Renderer->Bind(vao);
		m_Renderer->DrawElements(vao->GetSize(), PT_TRI);
	}

	m_Renderer->SetActiveFramebuffer();
}

void VolumeRenderer::RaytraceEmittance(Camera* cam, s32 index, s32 level)
{
	DrawTraceBounds(cam);

	// get camera position in volume coordinates
	vec3 pos = cam->GetPosition();
	pos = pos / vec3(m_WorldExtents.y);
	pos = vec3(0.5) * pos + vec3(0.5);

	m_Renderer->BindImage(0, m_Front->GetTexturePtr(), BA_READ);
	m_Renderer->BindImage(1, m_Back->GetTexturePtr(), BA_READ);
	m_Renderer->BindImage(2, m_TraceRT->GetTexturePtr(), BA_WRITE);

	m_Renderer->Bind(3, m_EmittanceVolumes[index]);

	m_Renderer->SetActiveShader(m_EmittanceTraceCS);
	m_EmittanceTraceCS->SetUniform(0, vec3(pos.x, pos.y, pos.z));
	m_EmittanceTraceCS->SetUniform(1, level);

	m_EmittanceTraceCS->Dispatch(m_Size.x / 8, m_Size.y / 4, 1);
	glMemoryBarrier(MB_IMAGE);
}

void VolumeRenderer::RaytraceOpacity(Camera* cam, s32 index, s32 level)
{
	DrawTraceBounds(cam);

	// get camera position in volume coordinates
	vec3 pos = cam->GetPosition();
	pos = pos / vec3(m_WorldExtents.y);
	pos = vec3(0.5) * pos + vec3(0.5);

	m_Renderer->BindImage(0, m_Front->GetTexturePtr(), BA_READ);
	m_Renderer->BindImage(1, m_Back->GetTexturePtr(), BA_READ);
	m_Renderer->BindImage(2, m_TraceRT->GetTexturePtr(), BA_WRITE);

	m_Renderer->Bind(3, m_OpacityVolumes[index]);

	m_Renderer->SetActiveShader(m_VolumeTraceCS);
	m_VolumeTraceCS->SetUniform(0, vec3(pos.x, pos.y, pos.z));
	m_VolumeTraceCS->SetUniform(1, level);

	m_VolumeTraceCS->Dispatch(m_Size.x / 8, m_Size.y / 4, 1);
	glMemoryBarrier(MB_IMAGE);
}

void VolumeRenderer::TraceAO(std::vector<TexturePtr> g_buffer, s32 nth_pixel, Camera* cam)
{

	m_Renderer->SetActiveShader(m_TraceAO);

	// bind normal texture
	m_Renderer->BindImage(0, g_buffer.at(1), BA_READ);

	// bind depth
	m_Renderer->Bind(1, g_buffer.at(2));

	for (int i = 0; i < 6; i++)
	{
		m_Renderer->Bind(i + 2, m_OpacityVolumes[i]);
	}
	

	m_Renderer->BindImage(3, m_AOResolve, BA_WRITE);
	mat4 inv_pv = glm::inverse(cam->GetProjectionMatrix()
		* cam->GetViewMatrix());

	m_TraceAO->SetUniform(0, MIPS - 1);
	m_TraceAO->SetUniform(1, m_WorldExtents.y);
	m_TraceAO->SetUniform(2, m_GridSize.x);
	m_TraceAO->SetUniform(3, vec2(m_Size));
	m_TraceAO->SetUniform(4, cam->GetPosition());
	m_TraceAO->SetUniform(5, inv_pv);

	m_TraceAO->Dispatch(m_Size.x / 8, m_Size.y / 4, 1);
	glMemoryBarrier(MB_IMAGE);
}

void VolumeRenderer::TraceDiffuseAO(std::vector<TexturePtr> g_buffer, s32 nth_pixel, Camera * cam)
{
	m_Renderer->SetActiveShader(m_TraceDiffuse);

	// bind normal texture
	m_Renderer->BindImage(1, g_buffer.at(0), BA_READ);
	m_Renderer->BindImage(2, g_buffer.at(1), BA_READ);

	// bind depth
	m_Renderer->Bind(3, g_buffer.at(2));


	for (int i = 0; i < 6; i++)
	{
		m_Renderer->Bind(i + 4, m_OpacityVolumes[i]);
		m_Renderer->Bind(i + 10, m_EmittanceVolumes[i]);
	}

	m_Renderer->BindImage(0, m_DiffuseResolve, BA_WRITE);
	mat4 inv_pv = glm::inverse(cam->GetProjectionMatrix()
		* cam->GetViewMatrix());

	m_TraceDiffuse->SetUniform(0, MIPS - 1);
	m_TraceDiffuse->SetUniform(1, m_WorldExtents.y);
	m_TraceDiffuse->SetUniform(2, m_GridSize.x);
	m_TraceDiffuse->SetUniform(3, vec2(m_Size));
	m_TraceDiffuse->SetUniform(4, cam->GetPosition());
	m_TraceDiffuse->SetUniform(5, inv_pv);

	m_TraceDiffuse->Dispatch(m_Size.x / 8, m_Size.y / 4, 1);
	glMemoryBarrier(MB_IMAGE);
}

void VolumeRenderer::TraceSpecular(std::vector<TexturePtr> g_buffer, s32 nth_pixel, Camera * cam)
{
	m_Renderer->SetActiveShader(m_TraceSpecular);

	// bind normal texture
	m_Renderer->BindImage(1, g_buffer.at(0), BA_READ);
	m_Renderer->BindImage(2, g_buffer.at(1), BA_READ);

	// bind depth
	m_Renderer->Bind(3, g_buffer.at(2));

	for (int i = 0; i < 6; i++)
	{
		m_Renderer->Bind(i + 4, m_OpacityVolumes[i]);
		m_Renderer->Bind(i + 10, m_EmittanceVolumes[i]);
	}

	m_Renderer->BindImage(0, m_SpecularResolve, BA_WRITE);
	mat4 inv_pv = glm::inverse(cam->GetProjectionMatrix()
		* cam->GetViewMatrix());

	m_TraceSpecular->SetUniform(0, MIPS - 1);
	m_TraceSpecular->SetUniform(1, m_WorldExtents.y);
	m_TraceSpecular->SetUniform(2, m_GridSize.x);
	m_TraceSpecular->SetUniform(3, vec2(m_Size));
	m_TraceSpecular->SetUniform(4, cam->GetPosition());
	m_TraceSpecular->SetUniform(5, inv_pv);

	m_TraceSpecular->Dispatch(m_Size.x / 8, m_Size.y / 4, 1);
	glMemoryBarrier(MB_IMAGE);
}

void VolumeRenderer::GatherIndirect(std::vector<TexturePtr> g_buffer, s32 nth_pixel, Camera * cam)
{
	m_Renderer->SetActiveShader(m_GatherIndirect);

	// bind normal texture
	m_Renderer->BindImage(1, g_buffer.at(0), BA_READ);
	m_Renderer->BindImage(2, g_buffer.at(1), BA_READ);

	// bind depth
	m_Renderer->Bind(3, g_buffer.at(2));

	for (int i = 0; i < 6; i++)
	{
		m_Renderer->Bind(i + 4, m_OpacityVolumes[i]);
		m_Renderer->Bind(i + 10, m_EmittanceVolumes[i]);
	}

	m_Renderer->BindImage(0, m_TraceRT->GetTexturePtr(), BA_WRITE);
	mat4 inv_pv = glm::inverse(cam->GetProjectionMatrix()
		* cam->GetViewMatrix());

	m_GatherIndirect->SetUniform(0, MIPS - 1);
	m_GatherIndirect->SetUniform(1, m_WorldExtents.y);
	m_GatherIndirect->SetUniform(2, m_GridSize.x);
	m_GatherIndirect->SetUniform(3, vec2(m_Size));
	m_GatherIndirect->SetUniform(4, cam->GetPosition());
	m_GatherIndirect->SetUniform(5, inv_pv);

	m_GatherIndirect->Dispatch(m_Size.x / 16, m_Size.y / 16, 1);
	glMemoryBarrier(MB_IMAGE);
}

void VolumeRenderer::DepthAwareBlur()
{
}

TexturePtr VolumeRenderer::GetResolvedTrace()
{
	return m_TraceRT->GetTexturePtr();
}

TexturePtr VolumeRenderer::GetResolvedAO()
{
	return m_AOResolve;
}

TexturePtr VolumeRenderer::GetResolvedDiffuse()
{
	return m_DiffuseResolve;
}

TexturePtr VolumeRenderer::GetResolvedSpecular()
{
	return m_SpecularResolve;
}

std::vector<TexturePtr> VolumeRenderer::GetOpacityVolumes()
{
	return m_OpacityVolumes;
}

std::vector<TexturePtr> VolumeRenderer::GetEmittanceVolumes()
{
	return m_EmittanceVolumes;
}

void VolumeRenderer::ClearVolume()
{
	rmt_ScopedOpenGLSample(ClearVolumes);
	m_Renderer->SetActiveShader(m_VolumeClear);
	std::vector<f32> d;
	d.push_back(0.0);

	if (m_OpacityDirty)
	{
		for (int i = 0; i < 6; i++)
		{
			m_Renderer->Clear(m_OpacityVolumes[i], &d[0]);
		}
	}
	

	d.push_back(0.0);
	d.push_back(0.0);
	d.push_back(0.0);

	if (m_EmittanceDirty)
	{
		for (int i = 0; i < 6; i++)
		{
			m_Renderer->Clear(m_EmittanceVolumes[i], &d[0]);
		}
	}
	glMemoryBarrier(MB_IMAGE);
}

void VolumeRenderer::SetVolumeExtents(vec4 ext)
{
	m_NegXCam->SetExtents(vec4(-ext.x, -ext.y, ext.z, ext.w));
	m_PosXCam->SetExtents(vec4(ext.x, ext.y, ext.z, ext.w));

	m_NegYCam->SetExtents(vec4(-ext.x, -ext.y, ext.z, ext.w));
	m_PosYCam->SetExtents(vec4(ext.x, ext.y, ext.z, ext.w));

	m_NegZCam->SetExtents(vec4(-ext.x, -ext.y, ext.z, ext.w));
	m_PosZCam->SetExtents(vec4(ext.x, ext.y, ext.z, ext.w));

	m_Cube->SetScale(vec3(ext.y));
	m_DebugCube->SetScale(vec3(ext.y));
	m_WorldExtents = ext;
}

void VolumeRenderer::SetNearPlane(f32 near)
{
	m_NegXCam->SetNearPlane(near);
	m_NegYCam->SetNearPlane(near);
	m_NegZCam->SetNearPlane(near);

	m_PosXCam->SetNearPlane(near);
	m_PosYCam->SetNearPlane(near);
	m_PosZCam->SetNearPlane(near);
}

void VolumeRenderer::SetFarPlane(f32 far)
{
	m_NegXCam->SetFarPlane(far);
	m_NegYCam->SetFarPlane(far);
	m_NegZCam->SetFarPlane(far);

	m_PosXCam->SetFarPlane(far);
	m_PosYCam->SetFarPlane(far);
	m_PosZCam->SetFarPlane(far);
}

void VolumeRenderer::SetVolumePosition(vec3 pos)
{
	m_NegXCam->SetPosition(vec3(m_WorldExtents.y, pos.y, pos.z));
	m_NegYCam->SetPosition(vec3(pos.x, m_WorldExtents.y, pos.z));
	m_NegZCam->SetPosition(vec3(pos.x, pos.y, m_WorldExtents.y));


	m_PosXCam->SetPosition(vec3(m_WorldExtents.x, pos.y, pos.z));
	m_PosYCam->SetPosition(vec3(pos.x, m_WorldExtents.x, pos.z));
	m_PosZCam->SetPosition(vec3(pos.x, pos.y, m_WorldExtents.x));
}

void VolumeRenderer::SetConservativeRaster(bool state)
{
	m_ConservativeRaster = state;
}

void VolumeRenderer::SetOpacityDirty(bool dirty)
{
	m_OpacityDirty = dirty;
}

void VolumeRenderer::SetEmittanceDirty(bool dirty)
{
	m_EmittanceDirty = dirty;
}

void VolumeRenderer::Blur(TexturePtr input, TexturePtr output, vec2 direction)
{
	ivec2 size = m_Renderer->GetOptions().WindowSize / 2;

	m_Renderer->SetActiveShader(m_BlurCS);

	m_Renderer->BindImage(0, output, BA_WRITE);
	m_Renderer->BindImage(1, input, BA_READ);

	m_BlurCS->Dispatch(size.x / 8, size.y / 4, 1);
	glMemoryBarrier(MB_IMAGE);
}

float VolumeRenderer::GetVoxelExtents()
{
	return m_WorldExtents.y;
}

s32 VolumeRenderer::GetVoxelResolution()
{
	return m_GridSize.x;
}

s32 VolumeRenderer::GetMipCount()
{
	return MIPS;
}

void VolumeRenderer::BindVolumes()
{
	for (int i = 0; i < 6; i++)
	{
		m_Renderer->Bind(i + 4, m_OpacityVolumes[i]);
		m_Renderer->Bind(i + 10, m_EmittanceVolumes[i]);
	}
}
