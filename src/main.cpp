#define GLM_FORCE_RADIANS
#include "PBRApp.h"

using namespace std::chrono;

int main(int argc, char** argv)
{

	PBRApp* app = new PBRApp();
	app->Init(argv[0]);
	app->Run();
	app->Destroy();
	return 0;
}