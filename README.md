### Setup ###

The project uses my GLFramework library as a base, which is contained in a submodule of this project.

Requires SDL2, ASSIMP, GLM, OpenGL 4.5, GLEW,

Build for x64 using CMake, the project has only been tested on Windows 10 using an NVIDIA GTX 1080.

Requires GL_NV_CONSERVATIVE_RASTER