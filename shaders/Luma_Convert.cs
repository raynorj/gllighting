#version 450 core
layout(local_size_x = 8, local_size_y = 4) in;

layout(rgba32f, binding = 0) uniform readonly image2D in_scene;
layout(r32f, binding = 1) uniform writeonly image2D out_luma;

uvec3 gid = gl_GlobalInvocationID;
uvec3 lid = gl_LocalInvocationID;
uvec3 group_size = gl_WorkGroupSize;

void main()
{
	vec2 pos = vec2(0.5) + vec2(gid.xy);
    ivec2 ipos = ivec2(pos);

    vec3 color = imageLoad(in_scene, ipos).rgb;
    float out_color = dot(color, vec3(0.299, 0.587, 0.114));

    imageStore(out_luma, ipos, vec4(out_color, 0.0, 0.0, 1.0));
}