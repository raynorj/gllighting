float shadow_sample(vec2 uv, float z, float bias)
{
    float depth = texture2D(tex_shadow, uv).r;
    return (z - bias < depth) ? 1.0 : 0.0;
}

float get_shadow(vec3 pos, vec3 N, vec3 dir)
{
    
    mat4 bias_mat = mat4(vec4(0.5, 0.0, 0.0, 0.5),
                     vec4(0.0, 0.5, 0.0, 0.5),
                     vec4(0.0, 0.0, 0.5, 0.5),
                     vec4(0.0, 0.0, 0.0, 1.0));
    vec4 light_space = (light_p * light_v) 
                //* inverse(p) 
                //* inv_v 
                * vec4(pos, 1.0);

    light_space /= light_space.w;
    light_space = light_space * 0.5 + 0.5;

    vec2 coords = light_space.xy;

    if(dot(N, dir) <= 0.0 || light_space.z > 1.0)
    {
       return 0.0;
    }

    float angle = max(0.0, dot(N, dir));

    //float offset  =N * 

    float bias = 0.000001 * tan(acos(angle));
    bias = max(min(bias, 0.0), 0.000005);

    float samples = 0.0;
    float NUM_SAMPLES = 12.0;
    int NUM_SAMPLES_2 = int(floor(NUM_SAMPLES / 2.0));
    vec2 offset = vec2(0.0);

    float texel_scale = 0.5;
    float texel = texel_scale / textureSize(tex_shadow, 0).x;



    for(int i = -NUM_SAMPLES_2; i <= NUM_SAMPLES_2; i++)
    {
        for(int j = -NUM_SAMPLES_2; j <= NUM_SAMPLES_2; j++)
        {
           offset = vec2(i, j) * texel;
           //samples += shadow_sample(coords + offset, light_space.z, bias);
           float depth = texture2D(tex_shadow, coords + offset).r;
           samples += (light_space.z - bias < depth) ? 1.0 : 0.0;
        }
    } 
    samples /= NUM_SAMPLES * NUM_SAMPLES;
    return samples;
}