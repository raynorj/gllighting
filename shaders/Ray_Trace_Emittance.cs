#version 450 core
#extension GL_NV_gpu_shader5 : enable

layout(local_size_x = 8, local_size_y = 4) in;

layout(rgba32f, binding = 0) uniform readonly image2D in_front;
layout(rgba32f, binding = 1) uniform readonly image2D in_back;

layout(rgba32f, binding = 2) uniform writeonly image2D out_resolved;

layout(binding = 3) uniform sampler3D in_volume;

layout(location = 0) uniform vec3 cam_pos;
layout(location = 1) uniform int level;

uvec3 gid = gl_GlobalInvocationID;
uvec3 lid = gl_LocalInvocationID;
uvec3 group_size = gl_WorkGroupSize;

vec4 trace(vec3 start, vec3 dir, vec3 step, int iters);

vec4 sample_volume(vec3 pos)
{
	return textureLod(in_volume, pos, level).rgba;
}

void main()
{
	vec2 pos = vec2(0.5) + vec2(gid.xy);
	ivec2 ipos = ivec2(pos);

	vec3 start = imageLoad(in_front, ipos).rgb;
	vec3 end = imageLoad(in_back, ipos).rgb;

	if(start == vec3(0.0) && end != start)
	{
		start = cam_pos;
	}

	vec3 dir = normalize(end - start);
	vec4 dest = f16vec4(0.0);

	int iters = 512 * 2;
	float len = length(end-start);
	float step_size = len / float(iters);

	vec3 step = dir * step_size;

	dest = trace(start, dir, step, iters);

	imageStore(out_resolved, ipos, dest);
}

vec4 trace(vec3 start, vec3 dir, vec3 step, int iters)
{
	vec4 dest = vec4(0.0);
	vec3 pos = start;

	for(int i = 0; i < iters; i++)
	{
		vec4 src = sample_volume(pos);

		src.rgb *= src.a;
		dest += (1.0 - dest.a) * src;

		if(any(greaterThan(src.rgb, vec3(0.0))))
		{
			//dest = src;
			//dest.a = 1.0;
			//break;
		}
		if(dest.a > 1.0)
		{
			dest.a = 1.0;
			break;
		}

		if(pos.x > 1.0 || pos.y > 1.0 || pos.z > 1.0)
		{
			break;
		}

		pos.xyz += step;
	}
	dest.rgb = pow(dest.rgb, vec3(1.0 / 2.2));
	return vec4(dest);
}