#version 450 core
layout(local_size_x = 4, local_size_y = 4, local_size_z = 4) in;

layout(rgba16f, binding = 0) uniform readonly image3D in_emittance;
layout(rgba16f, binding = 1) uniform writeonly image3D out_emittance;

uvec3 gid = gl_GlobalInvocationID;
uvec3 lid = gl_LocalInvocationID;
uvec3 group_size = gl_WorkGroupSize;

void main()
{
	ivec3 pos = ivec3(gid.xyz);
	vec4 val = imageLoad(in_emittance, pos);

	if(val.a > 1.0)
	{
		val.rgb /= val.a;
		val.a = 1.0;
	}
	imageStore(out_emittance, pos, val);
}