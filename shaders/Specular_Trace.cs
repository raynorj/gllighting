#version 450 core
layout(local_size_x = 8, local_size_y = 4) in;

uvec3 gid = gl_GlobalInvocationID;
uvec3 lid = gl_LocalInvocationID;
uvec3 group_size = gl_WorkGroupSize;

layout(rgba32f, binding = 1) uniform readonly image2D in_diffuse;
layout(rgba32f, binding = 2) uniform readonly image2D in_normal;
layout(binding = 3) uniform sampler2D in_depth;


layout(binding = 4) uniform sampler3D in_ONegX;
layout(binding = 5) uniform sampler3D in_ONegY;
layout(binding = 6) uniform sampler3D in_ONegZ;

layout(binding = 7) uniform sampler3D in_OPosX;
layout(binding = 8) uniform sampler3D in_OPosY;
layout(binding = 9) uniform sampler3D in_OPosZ;


layout(binding = 10) uniform sampler3D in_ENegX;
layout(binding = 11) uniform sampler3D in_ENegY;
layout(binding = 12) uniform sampler3D in_ENegZ;

layout(binding = 13) uniform sampler3D in_EPosX;
layout(binding = 14) uniform sampler3D in_EPosY;
layout(binding = 15) uniform sampler3D in_EPosZ;

layout(rgba32f, binding = 0) uniform writeonly image2D out_traced;


layout(location = 0) uniform int max_mip;
layout(location = 1) uniform float world_extents;
layout(location = 2) uniform int voxel_res;

layout(location = 3) uniform vec2 screen_size;

layout(location = 4) uniform vec3 cam_pos;
layout(location = 5) uniform mat4 inv_vp;

float min_cone_size = 1.0 / voxel_res;
float max_cone_size = world_extents * min_cone_size;

#include <Voxelize.inc>
#include <Cone_Trace.inc>

void main()
{
	ivec3 ipos = ivec3(gid.xyz);

	vec3 N = imageLoad(in_normal, ipos.xy).rgb;
	N = normalize(2.0 * N - 1.0);
	float depth = texelFetch(in_depth, ipos.xy, 0).r * 2.0 - 1.0;

	vec3 wpos = Get_Position(vec2(ipos.xy) / screen_size, depth, inv_vp);

	vec3 world_max = vec3(world_extents);
	vec3 world_min = -world_max;

	vec3 tex_pos = world_to_volume(wpos, world_max, world_min);
	vec3 tex_cam = world_to_volume(cam_pos, world_max, world_min);

	vec3 eye_dir = normalize(tex_pos - tex_cam);
	vec3 rdir = reflect(eye_dir, N);
	vec4 val = vec4(0.0);

	float r = imageLoad(in_diffuse, ipos.xy).r;
	float spec_cone = mix(1.0, 170.0,  1.0 - r);
	tex_pos += min_cone_size * N * sqrt(2.0) * 2;

	//if( gid.xy % ivec2(1) == 0)
	{
		ConeInfo info;
			info.Start = tex_pos;
			info.Dir = rdir;
			info.Cone_Angle = spec_cone;
			info.Dist_Offset = 2;
			info.Max_Dist = 0.25;
			info.Min_Cone = min_cone_size;
			info.Max_Cone = max_cone_size;
			info.Max_Mip = max_mip;
		val += trace(info);

		//val /= (PI / 4.0) + (w[1] * 5.0);
	}
	//val.rgb = reflect(eye_dir, N);
	imageStore(out_traced, ipos.xy, val);
}

TraceResult sample_volume(vec3 pos, vec3 dir, int lod)
{
	vec3 c = vec3(0.0);
	float ao = 0.0;
	TraceResult temp;
	vec3 w = abs(dir);
	//w = dir * dir;
	lod = max(lod - 1, 0);
	w *= (1.0 / (w.x + w.y + w.z + 0.0001));
	if(dir.x > 0.0)
	{
		c += w.x * textureLod(in_EPosX, pos, lod).rgb;
		ao += w.x * textureLod(in_OPosX, pos, lod).r;
	}
	else
	{
		c += w.x * textureLod(in_ENegX, pos, lod).rgb;
		ao += w.x * textureLod(in_ONegX, pos, lod).r;
	}

	if(dir.y > 0.0)
	{
		c += w.y * textureLod(in_EPosY, pos, lod).rgb;
		ao += w.y * textureLod(in_OPosY, pos, lod).r;
	}
	else
	{
		c += w.y * textureLod(in_ENegY, pos, lod).rgb;
		ao += w.y * textureLod(in_ONegY, pos, lod).r;
	}

	if(dir.z > 0.0)
	{
		c += w.z * textureLod(in_EPosZ, pos, lod).rgb;
		ao += w.z * textureLod(in_OPosZ, pos, lod).r;
	}
	else
	{
		c += w.z * textureLod(in_ENegZ, pos, lod).rgb;
		ao += w.z * textureLod(in_ONegZ, pos, lod).r;
	}
	
	temp.Occlusion = ao;
	temp.Color.rgb = c;

	return temp;
}