#version 450 core
layout(local_size_x = 8, local_size_y = 8) in;
precision highp float;
layout(rgba16f, binding = 0) coherent uniform readonly image2DArray in_diffusealpha;
layout(rgba32f, binding = 1) coherent uniform readonly image2DArray in_normaldepth;
layout(rg16f, binding = 2) coherent uniform readonly image2DArray in_roughnessmetal;
layout(r32ui, binding = 3) coherent uniform readonly uimage2D in_counter;

layout(rgba16f, binding = 4) uniform writeonly image2D out_resolved;
layout(rgba32f, binding = 5) uniform readonly image2D in_opaque;

layout(binding = 0) uniform sampler2D tex_shadow;

layout(binding = 4) uniform sampler3D in_ONegX;
layout(binding = 5) uniform sampler3D in_ONegY;
layout(binding = 6) uniform sampler3D in_ONegZ;
layout(binding = 7) uniform sampler3D in_OPosX;
layout(binding = 8) uniform sampler3D in_OPosY;
layout(binding = 9) uniform sampler3D in_OPosZ;

layout(binding = 10) uniform sampler3D in_ENegX;
layout(binding = 11) uniform sampler3D in_ENegY;
layout(binding = 12) uniform sampler3D in_ENegZ;
layout(binding = 13) uniform sampler3D in_EPosX;
layout(binding = 14) uniform sampler3D in_EPosY;
layout(binding = 15) uniform sampler3D in_EPosZ;

layout(location = 0) uniform int num_layers;

layout(location = 1) uniform float world_extents;
layout(location = 2) uniform int voxel_res;
layout(location = 3) uniform vec2 screen_size;
layout(location = 4) uniform int max_mip;


float min_cone_size = 1.0 / voxel_res;
float max_cone_size = world_extents * min_cone_size;


layout(location = 5) uniform vec3 cam_pos;
layout(location = 6) uniform vec3 cam_dir;
layout(location = 7) uniform mat4 inv_v;
layout(location = 8) uniform mat4 inv_p;
layout(location = 9) uniform mat4 inv_pv;

layout(location = 10) uniform vec3 light_pos;
layout(location = 11) uniform vec3 light_dir;
layout(location = 12) uniform vec3 light_color;
layout(location = 13) uniform float light_intensity;
layout(location = 14) uniform float light_radius;
layout(location = 15) uniform float light_cone;

layout(location = 16) uniform mat4 light_v;
layout(location = 17) uniform mat4 light_p;
layout(location = 18) uniform mat4 p;
layout(location = 19) uniform vec2 tile_offset;
#define NUM_LAYERS 12

#include <Shadows.inc>
#include <PBR.inc>
#include <Cone_Trace.inc>
#include <Depth.inc>

uvec3 gid = gl_GlobalInvocationID;
uvec3 lid = gl_LocalInvocationID;
uvec3 group_size = gl_WorkGroupSize;

struct Fragment
{
	vec3 diffuse;
	float a;
	vec3 normal;
	float depth;
	float roughness;
	float metal;
};

Fragment loc_fragments[NUM_LAYERS];

/*
	Sort fragment list
*/
void sort(uint layers)
{
	for(int i = 1; i < layers; i++)
	{
		Fragment key = loc_fragments[i];
		int j = i - 1;

		while(j >= 0 && loc_fragments[j].depth > key.depth)
		{
			loc_fragments[j + 1] = loc_fragments[j];
			j--;
		}

		loc_fragments[j + 1] = key;
	}
}

void load(ivec2 pos, uint layers)
{
	for(int i = 0; i < layers; i++)
	{
		vec4 da = imageLoad(in_diffusealpha, ivec3(pos, i));
		vec4 nd = imageLoad(in_normaldepth, ivec3(pos, i));
		vec2 rm = imageLoad(in_roughnessmetal, ivec3(pos, i)).rg;
		
		Fragment frag;

		frag.diffuse = da.rgb;
		frag.a = da.a;

		frag.normal = normalize(2.0 * nd.rgb - 1.0);
		frag.depth = nd.a * 2.0 - 1.0;

		frag.roughness = rm.r;
		frag.metal = rm.g;

		loc_fragments[i] = frag;
	}
}

const float INDIRECT_SCALE = 24.0;
vec4 blend(uint layers)
{
	vec4 color = vec4(0.0, 0.0, 0.0, 0.0);
	vec2 inv_size = 1.0 / vec2(imageSize(in_counter).xy);
	for(int i = 0; i < layers; i++)
	{
		Fragment frag = loc_fragments[i];

		vec4 col = vec4(0.0, 0.0, 0.0, frag.a);
		//col.rgb = frag.diffuse;
        vec2 uvs = (vec2(gid.xy) + tile_offset) * inv_size;
        
        vec3 pos = Get_Position(uvs, frag.depth);

        vec3 L = normalize(light_pos - pos);
        float dist = length(light_pos - pos);

        float NDL = max(0.0, dot(frag.normal, L));


		float att = Att_Point(NDL, dist, light_radius);

	    float cosD = dot(L, -normalize(light_dir));
		float shadow = get_shadow(pos, frag.normal, L);

	    if(cos(radians(light_cone)) > cosD)
	    {
	    	att = 0.0;
	    }
		//col.rgb *= att;

		//col.rgb *= shadow;
		//col.rgb = pos;

		vec3 world_max = vec3(world_extents);
		vec3 world_min = -world_max;
		vec3 tex_pos = world_to_volume(pos, world_max, world_min);
		vec3 tex_cam = world_to_volume(cam_pos, world_max, world_min);

		vec3 eye_dir = normalize(tex_pos - tex_cam);
		vec3 V = normalize(cam_pos - pos);
		//eye_dir = normalize(pos - cam_pos);
		vec3 rdir = reflect(eye_dir, frag.normal);
		ConeInfo info;
		info.Start = tex_pos;
		info.Dir = rdir;
		info.Cone_Angle = 20.0;
		info.Dist_Offset = 2.0;
		info.Max_Dist = 0.5;
		info.Min_Cone = min_cone_size;
		info.Max_Cone = max_cone_size;
		info.Max_Mip = max_mip;

		vec4 res = trace(info);
		col.rgb += frag.diffuse * res.rgb * res.a  * INDIRECT_SCALE;
		
		info.Dir = refract(eye_dir, frag.normal, 1.0 / 1.33);
		res = trace(info);
		col.rgb += frag.diffuse * res.rgb * res.a * INDIRECT_SCALE;

		//col.rgb += trace_diffuse(tex_pos, frag.normal).rgb * INDIRECT_SCALE;
        col.rgb *= col.a;
		color += col * (1.0 - color.a);
	}
	vec4 bg = imageLoad(in_opaque, ivec2(gid.xy)  + ivec2(tile_offset));
	//bg.rgb = vec3(1.0);
	//bg.a = 0.0;
	color += bg * (1.0 - color.a);
	return color;
}
void main()
{
    ivec2 ipos = ivec2(gid.xy) + ivec2(tile_offset);

    uint layers = min(num_layers, imageLoad(in_counter, ipos).r);
    vec4 color = imageLoad(in_opaque, ipos);
    color.a = 0.0;
    //color = vec4(1.0, 1.0, 1.0, 0.0);
    if(layers > 0)
    {
    	load(ipos, layers);
    	sort(layers);
    	color = blend(layers);
    }
    
    imageStore(out_resolved, ipos, color);
}