#version 450 core
layout(local_size_x = 16, local_size_y = 16) in;

uvec3 gid = gl_GlobalInvocationID;
uvec3 lid = gl_LocalInvocationID;
uvec3 group_size = gl_WorkGroupSize;

layout(rgba32f, binding = 1) uniform readonly image2D in_diffuse;
layout(rgba32f, binding = 2) uniform readonly image2D in_normal;
layout(binding = 3) uniform sampler2D in_depth;

layout(binding = 4) uniform sampler3D in_ONegX;
layout(binding = 5) uniform sampler3D in_ONegY;
layout(binding = 6) uniform sampler3D in_ONegZ;
layout(binding = 7) uniform sampler3D in_OPosX;
layout(binding = 8) uniform sampler3D in_OPosY;
layout(binding = 9) uniform sampler3D in_OPosZ;

layout(binding = 10) uniform sampler3D in_ENegX;
layout(binding = 11) uniform sampler3D in_ENegY;
layout(binding = 12) uniform sampler3D in_ENegZ;
layout(binding = 13) uniform sampler3D in_EPosX;
layout(binding = 14) uniform sampler3D in_EPosY;
layout(binding = 15) uniform sampler3D in_EPosZ;

layout(rgba32f, binding = 0) uniform writeonly image2D out_traced;

layout(location = 0) uniform int max_mip;
layout(location = 1) uniform float world_extents;
layout(location = 2) uniform int voxel_res;

layout(location = 3) uniform vec2 screen_size;

layout(location = 4) uniform vec3 cam_pos;
layout(location = 5) uniform mat4 inv_vp;

float min_cone_size = 1.0 / voxel_res;
float max_cone_size = world_extents * min_cone_size;

#include <Voxelize.inc>
#include <Cone_Trace.inc>

vec3 Fresnel_Schlick(float cosT, vec3 f)
{
    return f + (1.01f - f) * pow(1.0f - cosT, 5.0f);
}

const float INDIRECT_SCALE = 18.0;
void main()
{
	ivec3 ipos = ivec3(gid.xyz);
	vec4 norm = imageLoad(in_normal, ipos.xy);
	vec3 N = norm.rgb;
	N = normalize(2.0 * N - 1.0);
	float depth = texelFetch(in_depth, ipos.xy, 0).r * 2.0 - 1.0;

	vec3 wpos = Get_Position(vec2(ipos.xy) / screen_size, depth, inv_vp);

	vec3 world_max = vec3(world_extents);
	vec3 world_min = -world_max;

	vec3 tex_pos = world_to_volume(wpos, world_max, world_min);
	vec3 tex_cam = world_to_volume(cam_pos, world_max, world_min);
	vec3 eye_dir = normalize(tex_pos - tex_cam);
	vec4 val = vec4(0.0);

	vec4 d = imageLoad(in_diffuse, ipos.xy);
	float r =  d.a;
	float m = norm.a;

	float spec_cone = mix(10.0, 30.0, 1.0-r);

	tex_pos += min_cone_size * N * 1.0;// + (2.0 * bayer[ipos.x % 4][ipos.y % 4]);

	vec3 V = normalize(cam_pos - wpos);
    float NDV = max(dot(N, V), 0.0f);

    vec3 F0 = vec3(0.04); 
    F0 = mix(F0, d.rgb, vec3(m));

    vec3 F    = Fresnel_Schlick(NDV, F0);

	vec4 dest = vec4(0.0);
	dest += trace_diffuse(tex_pos, N) * INDIRECT_SCALE;// * vec4(vec3(1.0) - F, 1.0);
	vec3 rdir = reflect(eye_dir, N);

	ConeInfo info;
			info.Start = tex_pos;
			info.Dir = rdir;
			info.Cone_Angle = spec_cone;
			info.Dist_Offset = 2.0;
			info.Max_Dist = 0.3;
			info.Min_Cone = min_cone_size;
			info.Max_Cone = max_cone_size;
			info.Max_Mip = max_mip;

	// otherwise speckles show up with these textures on the hanging flags
	if(m > 0.2)
	{
		vec4 temp = trace(info) * INDIRECT_SCALE;
		dest += temp;

	}
	//dest *= INDIRECT_SCALE;
	imageStore(out_traced, ipos.xy, dest);
}
