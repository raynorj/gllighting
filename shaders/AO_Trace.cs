#version 450 core
layout(local_size_x = 8, local_size_y = 4) in;

uvec3 gid = gl_GlobalInvocationID;
uvec3 lid = gl_LocalInvocationID;
uvec3 group_size = gl_WorkGroupSize;

layout(rgba32f, binding = 0) uniform readonly image2D in_normal;
layout(binding = 1) uniform sampler2D in_depth;

layout(binding = 2) uniform sampler3D in_ONegX;
layout(binding = 3) uniform sampler3D in_ONegY;
layout(binding = 4) uniform sampler3D in_ONegZ;

layout(binding = 5) uniform sampler3D in_OPosX;
layout(binding = 6) uniform sampler3D in_OPosY;
layout(binding = 7) uniform sampler3D in_OPosZ;

layout(binding = 10) uniform sampler3D in_ENegX;
layout(binding = 11) uniform sampler3D in_ENegY;
layout(binding = 12) uniform sampler3D in_ENegZ;

layout(binding = 13) uniform sampler3D in_EPosX;
layout(binding = 14) uniform sampler3D in_EPosY;
layout(binding = 15) uniform sampler3D in_EPosZ;


layout(rgba32f, binding = 3) uniform writeonly image2D out_traced;

layout(location = 0) uniform int max_mip;
layout(location = 1) uniform float world_extents;
layout(location = 2) uniform int voxel_res;

layout(location = 3) uniform vec2 screen_size;

layout(location = 4) uniform vec3 cam_pos;
layout(location = 5) uniform mat4 inv_vp;

float min_cone_size = 1.0 / voxel_res;
float max_cone_size = world_extents * min_cone_size;

#include <Voxelize.inc>
#include <Cone_Trace.inc>

void main()
{
	ivec3 ipos = ivec3(gid.xyz);
	vec3 N = normalize(2.0 * imageLoad(in_normal, ipos.xy).rgb - 1.0);
	float depth = texelFetch(in_depth, ipos.xy, 0).r * 2.0 - 1.0;
	vec3 wpos = Get_Position(vec2(ipos.xy) / screen_size, depth, inv_vp);

	vec3 world_max = vec3(world_extents);
	vec3 world_min = -world_max;
	vec3 tex_pos = world_to_volume(wpos, world_max, world_min);
	vec3 tex_cam = world_to_volume(cam_pos, world_max, world_min);
	vec3 eye_dir = normalize(tex_pos - tex_cam);

	tex_pos += N * min_cone_size * 1.0;
	vec4 val = trace_diffuse(tex_pos, N).aaaa;

	imageStore(out_traced, ipos.xy, val);
}