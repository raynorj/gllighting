#version 450 core
layout(local_size_x = 8, local_size_y = 4) in;


layout(rgba32f, binding = 0) uniform readonly image2D in_diffuse;
layout(rgba32f, binding = 1) uniform readonly image2D in_direct;
layout(rgba32f, binding = 2) uniform readonly image2D in_indirect;

layout(rgba32f, binding = 3) uniform writeonly image2D out_lit;

uvec3 gid = gl_GlobalInvocationID;
uvec3 lid = gl_LocalInvocationID;
uvec3 group_size = gl_WorkGroupSize;

void main()
{
	vec2 pos = vec2(0.5) + vec2(gid.xy);
    ivec2 ipos = ivec2(pos);

    vec3 color = imageLoad(in_diffuse, ipos).rgb;
    vec3 direct = imageLoad(in_direct, ipos).rgb;

    vec4 indirect = imageLoad(in_indirect, ipos);

    vec4 final = vec4(0.0);
    final.a = 1.0;
    final.rgb = color * direct;

    if(!any(greaterThan(direct, vec3(0.0))))
    {
    	//indirect.rgb /= indirect.a;
    }
    final.rgb += color.rgb * indirect.rgb;
    //color.rgb = indirect.rgb;
    imageStore(out_lit, ipos, final);
}