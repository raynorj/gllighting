//const float near = 0.1f;
//const float far = 2000.0f;

float linearDepth(float z, float near, float far)
{
    z = z * 2.0 - 1.0;
    return (2.0 * near) / (far + near - z * (far - near));
}

float depthSample(float z, float near, float far)
{
    return (far + near - 2.0 * near * far / z) / (far - near);
}