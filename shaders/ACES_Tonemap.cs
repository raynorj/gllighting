#version 450 core
layout(local_size_x = 8, local_size_y = 4) in;

layout(rgba32f, binding = 0) uniform readonly image2D in_hdr;
layout(rgba32f, binding = 1) uniform readonly image2D in_luma;

layout(rgba32f, binding = 2) uniform writeonly image2D out_ldr;

#include <ACES.inc>

uvec3 gid = gl_GlobalInvocationID;
uvec3 lid = gl_LocalInvocationID;
uvec3 group_size = gl_WorkGroupSize;

void main()
{
	vec2 pos = vec2(0.5) + vec2(gid.xy);
    ivec2 ipos = ivec2(pos);

    vec3 color = imageLoad(in_hdr, ipos).rgb;
    float luma = imageLoad(in_luma, ipos).r;
	//color = max(vec3(0.0), color);
    //color *= exp(luma);
    #if 1
    color = ACESFitted(color);
    #else
    color = color / (1.0 + color);
    #endif
    color = pow(color, vec3(1.0 / 2.2)); 
  	
    imageStore(out_ldr, ipos, vec4(color, 1.0));
}