#ifndef PI
#define PI 3.14159265359
#endif
struct PointLight
{
   vec4 Position;
   vec3 Color;
   float Intensity;
   float Radius;
};

vec3 Get_Position(vec2 uv, float depth)
{
    vec4 position = vec4(uv * 2.0f - 1.0f, depth, 1.0f);
    position = inv_pv * position;

    return position.xyz / position.w;

    //return vec4(texture(tex_pos, uvs).rgb, 1.0f);
}

// n : normal
// h : half vector
// r : roughness
float D_GGX(vec3 n, vec3 h, float r)
{
	float a = r * r;
	float a_2 = a * a;

	float N_H = max(dot(n, h), 0.0f);
	float N_H_2 = N_H * N_H;

	float nom = a_2;
	float rest = (N_H_2 * (a_2 - 1.0f) + 1);

	float denom = PI * (rest * rest);

	return nom / denom;
}

vec3 Fresnel_Schlick(float cosT, vec3 f)
{
    float nv = 1.0 - cosT;
    float nv2 = nv * nv;
    nv2 *= nv2;
    return f + (1.0f - f) * (nv * nv2);
}

float G_SchlickGGX(float N_V, float r)
{
    float r_1 = (r + 1.0f);
    float k = (r_1 * r_1) / 8.0f;

    float nom = N_V;
    float denom = N_V * (1.0f - k) + k;
	
    return nom / denom;
}

float G_Smith(float NdotL, float NdotV, float r)
{
    float ggx2  = G_SchlickGGX(NdotV, r);
    float ggx1  = G_SchlickGGX(NdotL, r);
	
    return ggx1 * ggx2;
}

float Att_Directional(float N_L)
{
    return  N_L;
}

// http://blog.selfshadow.com/publications/s2013-shading-course/karis/s2013_pbs_epic_notes_v2.pdf
float Att_Dist_Unreal(float distance, float radius)
{
    // (dist/rad)^4
    float d_r = distance / radius;
    d_r = d_r * d_r;
    d_r = d_r * d_r;

    float s = clamp(1.0 - d_r, 0.0, 1.0);
    s = s * s;
    return s / ((distance * distance) + 1);
}

float Att_Point(float N_L, float distance, float radius)
{
    //return N_L * (1.0f / distance * distance);
    return N_L * Att_Dist_Unreal(distance, radius);
}

float Att_Spot(float N_L, float distance, float inner, float outer, float theta)
{
    float a = N_L * (1.0f / distance * distance);
    //a = N_L;
    float cosOuter = cos(radians(outer));
    float cosHalfOuter = cos(radians(outer * 0.5f));
    float cosInner = cos(radians(inner));

    float cosBothHalf = cos((radians(outer) + radians(outer) / 2));
    float cosTheta = theta; //cos(theta);
    float intense = light_intensity * 2.0f * PI * (1.0f - 0.5f * (cosInner + cosOuter));

    //intense = light_intensity * 2.0f * PI * (1.0f - cosBothHalf);
    if(cosTheta < cosOuter)
    {
        return 0.001f;
    }
    else if (cosTheta > cosInner)
    {
        return a * intense;
    }

    float delta = (cosTheta - cosOuter) / (cosInner - cosOuter);
    //delta = cosHalfOuter;
    float b = a * delta * delta * delta * delta;

    return b * intense;

}

vec3 Calculate_Lighting(vec3 L, vec3 cam_pos, vec3 pos, 
                      vec3 N, float roughness, vec3 diffuse, 
                      float metallic)
{
    vec3 V = normalize(cam_pos - pos);
    vec3 H = normalize(V + L);

    float NDL = max(dot(N, L), 0.0f);
    float NDV = max(dot(N, V), 0.0f);

    vec3 F0 = vec3(0.04); 
    F0 = mix(F0, diffuse, vec3(metallic));

    vec3 F    = Fresnel_Schlick(NDV, F0);
    float NDF = D_GGX(N, H, roughness);        
    float G   = G_Smith(NDL, NDV, roughness);      

    vec3 kS = F;
    vec3 kD = vec3(1.0f) - kS;
    kD *= 1.0f - metallic;    
        
    vec3 nominator = NDF * G * F;
    float denominator = 4.0f * NDV * NDL + 0.001f; 
    vec3 brdf = nominator / denominator;

    //vec3 radiance = light_color * light_intensity * att;  

    vec3 color = (kD * diffuse / PI + brdf) * get_shadow(pos,N, L);  

    return color;
}

vec3 Calculate_Lighting_Diffuse(vec3 L, vec3 cam_pos, vec3 pos, 
                      vec3 N, float roughness, vec3 diffuse, 
                      float metallic)
{
    vec3 V = normalize(cam_pos - pos);
    vec3 H = normalize(V + L);

    float NDL = max(dot(N, L), 0.0f);
    float NDV = max(dot(N, V), 0.0f);

    vec3 F0 = vec3(0.04); 
    F0 = mix(F0, diffuse, vec3(metallic));

    vec3 F    = Fresnel_Schlick(NDV, F0);
    vec3 kD = vec3(1.0f) - F;
    kD *= 1.0f - metallic;            

    vec3 color = (kD * diffuse / PI);  

    return color;
}