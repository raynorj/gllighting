#version 450 core


layout(location = 0) in vec3 in_position;
layout(location = 1) in vec2 in_uvs;

layout(location = 0) out vec3 out_position;
layout(location = 1) out vec2 out_uvs;
out gl_PerVertex
{
	vec4 gl_Position;
};

void main()
{
	gl_Position = vec4(in_position, 1.0f);
	out_uvs = in_uvs;
	out_position = in_position;
}