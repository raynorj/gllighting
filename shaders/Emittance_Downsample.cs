#version 450 core
layout(local_size_x = 4, local_size_y = 4, local_size_z = 4) in;

layout(binding = 0) uniform sampler3D in_layer;
//layout(rgba16f, binding = 0) uniform readonly image3D in_layer;
layout(rgba16f, binding = 1) uniform writeonly image3D out_layer;


uvec3 gid = gl_GlobalInvocationID;
uvec3 lid = gl_LocalInvocationID;
uvec3 group_size = gl_WorkGroupSize;

layout(location = 0) uniform int level;
layout(location = 1) uniform int dir;
layout(location = 2) uniform int scale;

#include <Voxelize.inc>

void main()
{
	ivec3 dest_pos = ivec3(gid.xyz);
	ivec3 src_pos = dest_pos * ivec3(2);

#if 1
	ivec3 pos[8] = {
			ivec3(0, 0, 0),
			ivec3(1, 0, 0),
			ivec3(0, 1, 0),
			ivec3(1, 1, 0),
			ivec3(0, 0, 1),
			ivec3(1, 0, 1),
			ivec3(0, 1, 1),
			ivec3(1, 1, 1),
	};
#else
	ivec3 pos[8] = {
			ivec3(1, 1, 1),
			ivec3(0, 1, 1),
			ivec3(1, 0, 1),
			ivec3(0, 0, 1),
			ivec3(1, 1, 0),
			ivec3(0, 1, 0),
			ivec3(1, 0, 0),
			ivec3(0, 0, 0),
	};
#endif

	vec4 v[8];
	float count = 0.0;
	vec4 final = vec4(0.0);
	for(int i = 0; i < 8; i++)
	{
		v[i] = texelFetch(in_layer, src_pos + pos[i], level);
		if(any(greaterThan(v[i], vec4(0.0))))
		{
			final += v[i];
			count += 1.0;
		}
		
	}

	vec4 mip = mipmap_anisotropic(v, dir);
	if(count > 0.0)
	{
		//mip = final / count;
		//mip.a = 1.0;
	}
	
	//mip = min(vec4(1.0), mip);
	imageStore(out_layer, dest_pos, mip);
}