#define NEG_X 0
#define NEG_Y 1
#define NEG_Z 2
#define POS_X 3
#define POS_Y 4
#define POS_Z 5

vec3 adjust_position(vec3 pos, ivec3 grid_size, int dir)
{
	if(dir == NEG_X)
	{
		pos.xyz = vec3(grid_size.x - pos.z, pos.y, pos.x);
	}
	else if (dir == NEG_Y)
	{
		pos.xyz = vec3(grid_size.x - pos.x, grid_size.x - pos.z, grid_size.x - pos.y);
	}
	else if (dir == NEG_Z)
	{
		pos.xyz = vec3(grid_size.x - pos.x, pos.y, grid_size.z - pos.z);
	}
	else if(dir == POS_X)
	{
		pos.xyz = vec3(pos.z, pos.y, pos.x);
	}
	else if(dir == POS_Y)
	{
		pos.xyz = vec3(pos.x, pos.z, pos.y);
	}
	else if(dir == POS_Z)
	{
		pos.xyz = vec3(grid_size.x - pos.x, pos.y, pos.z);
	}
	return pos;
}

vec3 Get_Position(vec2 uv, float depth, mat4 pv)
{
    vec4 position = pv * vec4(uv * 2.0f - 1.0f, depth, 1.0f);
    return position.xyz / position.w;
}

vec4 blend(vec4 a, vec4 b)
{
	return a + b * (1.0 - a.a);
}

vec4 mipmap_anisotropic(vec4 v[8], int dir)
{
	vec4 final = vec4(0.0);

	if(dir == NEG_X)
	{
		final = blend(v[1], v[0]) + blend(v[3], v[2]) + blend(v[5], v[4]) + blend(v[7], v[6]);
	}
	else if(dir == POS_X)
	{
		final = blend(v[0], v[1]) + blend(v[2], v[3]) + blend(v[4], v[5]) + blend(v[6], v[7]);
	}
	else if(dir == NEG_Y)
	{
		final = blend(v[2], v[0]) + blend(v[3], v[1]) + blend(v[6], v[4]) + blend(v[7], v[5]);
	}
	else if(dir == POS_Y)
	{
		final = blend(v[0], v[2]) + blend(v[1], v[3]) + blend(v[4], v[6]) + blend(v[5], v[7]);
	}
	else if(dir == NEG_Z)
	{
		final = blend(v[4], v[0]) + blend(v[5], v[1]) + blend(v[6], v[2]) + blend(v[7], v[3]);
	}
	else if(dir == POS_Z)
	{
		final = blend(v[0], v[4]) + blend(v[1], v[5]) + blend(v[2], v[6]) + blend(v[3], v[7]);
	}

	return final * 0.25;
}
