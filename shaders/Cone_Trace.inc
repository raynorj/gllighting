#define SQRT_2 sqrt(2.0)
#ifndef PI
#define PI 3.1415926535
#endif
mat4 bayer = mat4(vec4(0.0, 0.5, 2.0 / 16.0, 10.0 / 16.0),
				  vec4(12.0 / 16.0, 0.25, 14.0 / 16.0, 6.0 / 16.0),
				  vec4(3.0 / 16.0, 11.0 / 16.0, 1.0 / 16.0, 9.0 / 16.0),
				  vec4(15.0 / 16.0, 7.0 / 16.0, 13.0 / 16.0, 5.0 / 16.0));

float random (in vec2 p) 
{
    return fract(sin(dot(p.xy, vec2(12.9898, 78.233))) * 43758.5453123);
}

vec3 world_to_volume(vec3 w, vec3 world_max, vec3 world_min)
{
	return ((w - world_min) / (world_max - world_min));
}

vec3 ortho(vec3 v)
{
	v = normalize(v);
	vec3 u = vec3(0.0, 1.0, 0.0);
	return abs(dot(u, v)) > 0.999 ? cross(u, vec3(0, 0, 1)) : cross(u, v);
}


vec3 project_n(vec3 N, vec3 n1)
{
	N = normalize(N);

	vec3 T = normalize(ortho(N));
	vec3 B = normalize(cross(N, T));
	mat3 m = mat3(T, B, N);

	return normalize(n1 * m);
}

#define NUM_CONES 9
vec3[NUM_CONES] get_cones(vec3 N)
{
	vec3 T = normalize(ortho(N));
	vec3 B = normalize(cross(N, T));
	#if NUM_CONES == 4
	vec3 cones[NUM_CONES] = {
		N+T,
		N-T,
		N+B,
		N-B
	};
	#elif NUM_CONES == 6
	vec3 cones[NUM_CONES] = {
		vec3(0.0, 1.0, 0.0),
		vec3(0.0, 0.5, 0.866025),
		vec3(0.823639, 0.5, 0.267617),

		vec3(0.509037, 0.5, -0.700629),
		vec3(-0.509037, 0.5, -0.700629),
		vec3(-0.823639, 0.5, 0.267617)
	};//*/
	#elif NUM_CONES == 5
	vec3 cones[NUM_CONES] = {
		N, 
		N+T,
		N+B,
		N-T,
		N-B
	};//*/

	#elif NUM_CONES == 9
	vec3 cones[NUM_CONES] = {
		N,

		N+T,
		N+B,
		N-T,
		N-B,

		N+(T+B),
		N+(T-B),
		N-(T+B),
		N-(T-B)
	};
	#elif NUM_CONES == 10
	vec3 cones[NUM_CONES] = {
		N,

		N+T,
		N+B,
		N-T,
		N-B,

		-N,

		-N+T,
		-N+B,
		-N-T,
		-N-B,
	};
	#elif NUM_CONES == 14

	vec3 X = vec3(1.0, 0.0, 0.0);
	vec3 Y = vec3(0.0, 1.0, 0.0);
	vec3 Z = vec3(0.0, 0.0, 1.0);
	vec3 cones[NUM_CONES] = {
		X, -X,
		Y, -Y,
		Z, -Z,

		Y + X, Y - X,
		Y + Z, Y - Z,

		Y + X + Z, Y - X + Z,
		Y + X - Z, Y - X - Z
	};
	#endif

	for(int i = 0; i < NUM_CONES; i++)
	{
		cones[i] = normalize(cones[i]);

	#if NUM_CONES == 6
		cones[i] = project_n(N, cones[i].xyz);
	#endif
	}
	return cones;
}

float[NUM_CONES] get_weights()
{
	float w0 = 1.0 / float(NUM_CONES);

	float[NUM_CONES] w;

	float w1 = (3.0*PI) / 20;

	
	for(int i = 0; i < NUM_CONES; i++)
	{
		w[i] = w0;

		#if NUM_CONES == 6
		w[i] = w1;

		#elif NUM_CONES == 5
		w[i] = 0.707;
		#endif
	}
	
	#if NUM_CONES == 6
	w[0] = PI / 4;
	#elif NUM_CONES == 5
	w[0] = 1.0;
	#endif

	return w;
}

struct TraceResult
{
	vec4 Color;
	float Occlusion;
};

vec4 sample_volume(vec3 pos, vec3 dir, float lod);

vec4 sample_diffuse(vec3 pos, vec3 dir, float lod);
float sample_ao(vec3 pos, vec3 dir, float lod);

struct ConeInfo
{
	vec3 Start;
	vec3 Dir;

	float Cone_Angle;
	float Dist_Offset;
	float Max_Dist;

	float Min_Cone;
	float Max_Cone;
	float Max_Mip;
};

vec4 trace(ConeInfo info)
{
	float cone_size = 0.0, lod = 0.0;
	float dist = info.Dist_Offset * info.Min_Cone;
	vec3 pos = info.Start;
	info.Dir = normalize(info.Dir);
	float cone_tan_angle = tan(radians(info.Cone_Angle * 0.5));
	float step = 1.0;
	float scaled_dist = 1.0;
	float min_inv = 1.0 / info.Min_Cone;

	vec4 dest = vec4(0.0);
	int iters = 0;
	while(dist < info.Max_Dist && dest.a < 1.0)
	{
		//cone_size = info.Cone_Angle * dist;
		cone_size = 2.0 * dist * cone_tan_angle;
		cone_size = max(info.Min_Cone, cone_size);
		cone_size = min(info.Max_Cone, cone_size);

		lod = log2(cone_size * min_inv);
		lod = min(lod, info.Max_Mip);
		lod = max(lod, 0.0);
		pos = info.Start + (info.Dir * dist);
		vec4 src = sample_volume(pos, info.Dir, lod);

		float a = 1.0 - dest.a;
		

		#if 0
		float a_1 = 1.0 - pow(1.0 - src.a, scaled_dist / cone_size);
		dest.rgb += src.rgb * a_1 * a;
		dest.a += a_1 * a;
		#else
		dest.rgb += src.rgb * src.a * a;
		dest.a += src.a * a;
		#endif

		if(pos.x > 1.0 || pos.y > 1.0 || pos.z > 1.0)
		{
			break;
		}
		dist += cone_size * scaled_dist;
		iters++;

		if(iters > 130)
		{
			break;
		}
	}
	//val.Color *= dist;

	// occlusion weight decay
	float lambda = 1e-4;
	dest.a *= 1.0 / ( 1.0 + lambda * dist);
	return dest;
}

vec4 trace_diffuse(vec3 start, vec3 N)
{
	vec4 color = vec4(0.0);

	vec3 cones[NUM_CONES] = get_cones(N);
	float w[NUM_CONES] = get_weights();

	for(int i = 0; i < NUM_CONES; i++)
	{
		ConeInfo info;
		info.Start = start;
		info.Dir = cones[i];
		info.Cone_Angle = 2.0 * 180.0 / NUM_CONES;
		info.Dist_Offset = 2.0;
		info.Max_Dist = 0.25;
		info.Min_Cone = min_cone_size;
		info.Max_Cone = max_cone_size;
		info.Max_Mip = max_mip;

		vec4 res = trace(info);
		color.rgb += w[i] * res.rgb;
		color.a += w[i] * (1.0 - res.a);
	}
	return color;
}

vec3 trace_specular(vec3 start, vec3 N, float IOR)
{
	vec3 color = vec3(0.0);
	return color;
}

float sample_opacity(vec3 pos, vec3 dir, vec3 w, float lod)
{
	float ret = 0.0;

	if(dir.x > 0.0)
		{ ret += w.x * textureLod(in_OPosX, pos, lod).r; }
	else
		{ ret += w.x * textureLod(in_ONegX, pos, lod).r; }

	if(dir.y > 0.0)
		{ ret += w.y * textureLod(in_OPosY, pos, lod).r; }
	else
		{ ret += w.y * textureLod(in_ONegY, pos, lod).r; }

	if(dir.z > 0.0)
		{ ret += w.z * textureLod(in_OPosZ, pos, lod).r; }
	else
		{ ret += w.z * textureLod(in_ONegZ, pos, lod).r; }

	return ret;
}

vec3 sample_emittance(vec3 pos, vec3 dir, vec3 w, float lod)
{
	vec3 ret = vec3(0.0);

	if(dir.x > 0.0)
		{ ret += w.x * textureLod(in_EPosX, pos, lod).rgb; }
	else
		{ ret += w.x * textureLod(in_ENegX, pos, lod).rgb; }

	if(dir.y > 0.0)
		{ ret += w.y * textureLod(in_EPosY, pos, lod).rgb; }
	else
		{ ret += w.y * textureLod(in_ENegY, pos, lod).rgb; }

	if(dir.z > 0.0)
		{ ret += w.z * textureLod(in_EPosZ, pos, lod).rgb; }
	else
		{ ret += w.z * textureLod(in_ENegZ, pos, lod).rgb; }

	return ret;
}
vec4 sample_volume(vec3 pos, vec3 dir, float lod)
{
	vec3 w = abs(dir);
	//w = dir * dir;
	w *= (1.0 / (w.x + w.y + w.z + 0.0001));
	return vec4(sample_emittance(pos, -dir, w, lod),
				  sample_opacity(pos, dir, w, lod));
}