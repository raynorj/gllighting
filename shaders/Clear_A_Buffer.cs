#version 450 core
layout(local_size_x = 8, local_size_y = 4) in;

layout(rgba32f, binding = 0) uniform writeonly image2DArray out_diffusealpha;
layout(rgba32f, binding = 1) uniform writeonly image2DArray out_normaldepth;
layout(rg32f, binding = 2) uniform writeonly image2DArray out_roughnessmetal;
layout(r32ui, binding = 3) uniform writeonly uimage2D out_counter;
layout(rgba32f, binding = 4) uniform writeonly image2D out_resolved;


layout(location = 0) uniform int num_layers;

uvec3 gid = gl_GlobalInvocationID;
uvec3 lid = gl_LocalInvocationID;
uvec3 group_size = gl_WorkGroupSize;

void main()
{

	vec2 pos = vec2(gid.xy);
    ivec2 ipos = ivec2(pos);
    //ipos = ivec2(gid.xy);
    for(int i = 0; i < num_layers; i++)
    { 
        ivec3 p = ivec3(ipos.x, ipos.y, i);
        imageStore(out_diffusealpha, p, vec4(0.0));
        imageStore(out_normaldepth, p, vec4(0.0));
        imageStore(out_roughnessmetal, p, vec4(0.0));
    }
    imageStore(out_counter, ipos, uvec4(0));
    imageStore(out_resolved, ipos, vec4(0.0));
}