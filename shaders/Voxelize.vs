#version 450 core

layout(location = 0) in vec3 in_position;
layout(location = 1) in vec2 in_uvs;
layout(location = 2) in vec3 in_normal;

layout(location = 0) out vec4 out_eye;
layout(location = 1) out vec2 out_uvs;
layout(location = 2) out vec4 out_normal;
layout(location = 3) out vec4 out_pos;

layout(location = 0) uniform mat4 m;
layout(location = 1) uniform mat4 v;
layout(location = 2) uniform mat4 p;

out gl_PerVertex
{
	vec4 gl_Position;
	float gl_PointSize;
	float gl_ClipDistance[];
};

void main()
{
	vec4 pos = p * v * m * vec4(in_position, 1.0f);
	out_pos = pos;
	gl_Position = pos;

	out_uvs = in_uvs;
	out_normal = m * vec4(in_normal, 0.0f);
	out_eye = m * vec4(in_position, 1.0f);
	//out_eye = pos;
}