#version 450 core

precision highp float;

layout(location = 0) in vec3 in_pos;
layout(location = 1) in vec2 in_uvs;
layout(location = 1) uniform mat4 light_v;
layout(location = 2) uniform mat4 light_p;
layout(location = 3) uniform mat4 model;

layout(location = 0) out vec2 out_uvs;

out gl_PerVertex
{
	vec4 gl_Position;
	float gl_PointSize;
	float gl_ClipDistance[];
};

void main()
{
	gl_Position = light_p * light_v * model * vec4(in_pos, 1.0);
	out_uvs = in_uvs;
}