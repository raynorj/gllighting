#version 450 core

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

layout(location = 0) in vec4 in_eye[];
layout(location = 1) in vec2 in_uvs[];
layout(location = 2) in vec4 in_normal[];
layout(location = 3) in vec4 in_pos[];


layout(location = 0) out vec4 out_eye;
layout(location = 1) out vec2 out_uvs;
layout(location = 2) out vec4 out_normal;
layout(location = 3) out mat3 out_swizzle;
layout(location = 6) out flat int dir;

layout(location = 0) uniform mat4 m;
layout(location = 1) uniform mat4 v;
layout(location = 2) uniform mat4 p;

layout(location = 3) uniform mat4 vp_x;
layout(location = 4) uniform mat4 vp_y;
layout(location = 5) uniform mat4 vp_z;

mat3 swizzle_x = mat3(vec3(0.0, 0.0, 1.0),
				   vec3(0.0, 1.0, 0.0),
				   vec3(1.0, 0.0, 0.0));

mat3 swizzle_y = mat3(vec3(1.0, 0.0, 0.0),
				   vec3(0.0, 0.0, 1.0),
				   vec3(0.0, 1.0, 0.0));

mat3 swizzle_z = mat3(vec3(1.0, 0.0, 0.0),
				   vec3(0.0, 1.0, 0.0),
				   vec3(0.0, 0.0, 1.0));

out gl_PerVertex
{
	vec4 gl_Position;
	float gl_PointSize;
	float gl_ClipDistance[];
};


void main()
{
	// get dominant axis
	vec3 n = abs(cross(in_pos[1].xyz - in_pos[0].xyz, 
						in_pos[2].xyz - in_pos[0].xyz));
	
	float axis = max(max(n.x, n.y), n.z);

	// project using swizzling
	mat3 swizzle;
	if(axis == n.x)
	{
		swizzle = swizzle_x;
	}
	else if(axis == n.y)
	{
		swizzle = swizzle_y;
	}
	else if(axis == n.z)
	{
		swizzle = swizzle_z;
	}

	out_swizzle = inverse(swizzle);

	// just output, no need for expansion 
	// because hardware supported conservative raster is enabled
	for(int i = 0; i < 3; i++)
	{
		vec4 pos = vec4(swizzle * in_pos[i].xyz, 1.0);
		out_eye = in_eye[i];
		out_normal = in_normal[i];
		out_uvs = in_uvs[i];
		gl_Position = pos;

		EmitVertex();
	}
    EndPrimitive();
}