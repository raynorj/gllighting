#version 450 core

layout(location = 0) in vec3 in_position;
layout(location = 1) in vec2 in_uvs;
layout(location = 2) in vec3 in_normal;

layout(location = 1) out vec2 out_uvs;
layout(location = 2) out vec4 out_normal;
layout(location = 3) out vec4 out_eye;

layout(location = 2) uniform mat4 mvp;
layout(location = 3) uniform mat4 mv;
layout(location = 4) uniform mat4 m;

out gl_PerVertex
{
	vec4 gl_Position;
	float gl_PointSize;
	float gl_ClipDistance[];
};

void main()
{
	gl_Position = mvp * vec4(in_position, 1.0f);

	out_uvs = in_uvs;
	out_normal = vec4(mat3(transpose(inverse(m))) * in_normal, 0.0f);
	out_eye = (m * vec4(in_position, 1.0f));
}