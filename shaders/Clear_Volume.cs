#version 450 core
layout(local_size_x = 4, local_size_y = 4, local_size_z = 2) in;

layout(rgba32f, binding = 0) uniform writeonly image3D out_quantity;

uvec3 gid = gl_GlobalInvocationID;
uvec3 lid = gl_LocalInvocationID;
uvec3 group_size = gl_WorkGroupSize;

void main()
{
	imageStore(out_quantity, ivec3(gid), vec4(0.0));
}